#include <stdio.h>

/* Count stuff in text streams  */

// Using constants for state instead of plain boolean for readability
#define IN 1 /* Inside a word */
#define OUT 0 /* Outside a word */

int main() {
	int c;
	float nl, nb, nt, nc, nw, state;

	c = nl = nb = nt = nc = nw = 0;
	state = OUT;

	while ((c = getchar()) != EOF) {
		nc++;
		switch (c){
			case '\n':
				nl++;
				state = OUT;
				break;
			case '\t':
				nt++;
				state = OUT;
				break;
			case ' ':
				nb++;
				state = OUT;
				break;
			default:
				nw++;
				state = IN;
				break;
		}
	}
	printf("%s%.0f\n", "Number of characters: ", nc);
	printf("%s%.0f\n", "Number of words: ", nw);
	printf("%s%.0f\n", "Number of lines: ", nl);
	printf("%s%.0f\n", "Number of tabs: ", nt);
	printf("%s%.0f\n", "Number of blanks:", nb);
}
