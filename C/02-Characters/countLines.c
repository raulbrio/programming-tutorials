#include <stdio.h>

/* Count lines in input  */

int main() {
	int c, n1 = 0;

	while ((c = getchar()) != EOF) {
		/* A character written between single quotes represents 
		 * an integer valure equal to the numerical value of the 
		 * character in the machine's character set. This is called
		 * a "character constant". */
		if (c == '\n'){
			n1++;
		}
	}
	printf("%s%d\n", "Number of lines: ", n1);
}
