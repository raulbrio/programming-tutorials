#include <stdio.h>

/* Count characters in input; 2nd version */

int main() {
	double nc;

	for (nc = 0; getchar() != EOF; ++nc) {
		; //for statement always needs a body
	}
	printf("%.0f\n", nc);
}
