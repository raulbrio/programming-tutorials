#include <stdio.h>

/* print Fahrenheit-Celsius table for fahr = 0, 20, ..., 300 */

/* Printf specifications:
 * %d		print as decimal integer
 * %6d		print as decimal integer, at least 6 characters wide
 * %f		print as floating point
 * %6f		print as floating point, at least 6 characters wide
 * %.2f		print as floating point, 2 characters after decimal point
 * %6.2f	print as floating point, at least 6 wide and 2 after after decimal point
 * %o		print as octal
 * %x		print as hexadecimal
 * %c		print as char
 * %s		print as String of char
 * %%		print the % character itself
 * \t		includes a tab
 * \n		includes a "new line" (charriage return). Mandatory as the end of a printf line*/

#define LOWER 0		// lower limit of temperature table
#define UPPER 300	// upper limit
#define STEP 20		// step size

int main() {
	float fahr, celsius;

	fahr = LOWER;

	printf("%s\t%s\n", "ºF", "ºC");
	printf("==========\n");

	while (fahr <= UPPER) {
		celsius = 5.0 * (fahr - 32.0) / 9.0;
		printf ("%3.0f %6.1f\n", fahr, celsius);
		fahr += STEP;
	}

	// now let's do the same with a for statement.
	printf("\n\n");
	for (fahr = LOWER; fahr <= UPPER; fahr += STEP) {
		printf("%3.0f %6.1f\n", fahr, (5.0/9.0) * (fahr-32));
	}
}
