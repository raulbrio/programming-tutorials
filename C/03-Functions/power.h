#include <stdio.h>

int power(int m, int n);

/* power: raise base to n-th power; n >= 0  */
int power(int base, int n) {
	int p;
	if (n < 0){
	printf("ERROR - Exponent must be > 0\n");
	return 1;
	}
	else {
		for (p = 1; n > 0; n--){
			p *= base;
		}
		return p;
	}
}
