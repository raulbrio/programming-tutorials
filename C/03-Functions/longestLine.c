#include <stdio.h>
#define MAXLINE 1000 /* Maximum input line size  */

/* Find the longest line in a text  */

int getLine(char line[], int maxLine);
void copyLine(char to[], char from[]);

int main(){
	int len;				// Current line lenght
	int max = 0;			// Maximum lenght seen so far
	char line[MAXLINE];		// Current input line
	char longest[MAXLINE];	// Longest line saved here

	// while there is a line (even empty lines contain character '\n')
	while ((len = getLine(line, MAXLINE)) > 0){ 
		if (len > max){
			max =len;
			copyLine(longest, line);
		}
	}
	if (max > 0){ // there was a line
		printf("%s", longest);
	}
	return 0;
}

/* getLine: read a line into s, return lenght  */
int getLine(char s[], int lim){
	int c, i;

	for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; i++){
		s[i] = c;
	}
	if (c == '\n'){
		s[i] = c;
		i++;
	}
	s[i] = '\0'; //mark the end of the character string
	return i;
}

/* copyLine: copy 'from' into 'to'; assume to is big enough  */
void copyLine(char to[], char from[]){
	for(int i = 0; (to[i] = from[i]) != '\0'; i++);
}
