#include <stdio.h>
#include "power.h"

int main(){
	int i;
	printf("Exp\t2^Exp\t-3^Exp\n");
	for (i = 0; i < 10; i++) {
		printf("%d\t%d\t%d\n", i, power(2,i), power(-3,i));
	}
	return 0;
}
