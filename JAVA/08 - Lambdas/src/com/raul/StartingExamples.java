package com.raul;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class StartingExamples {

    public static void main(String[] args) {

        Employee john = new Employee("John Doe", 30);
        Employee tim = new Employee("Tim Buchalka", 21);
        Employee jack = new Employee("Jack Hill", 40);
        Employee snow = new Employee("Snow White", 22);

        List<Employee> employees = new ArrayList<>();
        employees.add(john);
        employees.add(tim);
        employees.add(jack);
        employees.add(snow);

        // Let´s first sort the employees the classic way: override the method compare in a new Comparator object
        Collections.sort(employees, new Comparator<Employee>() {
            @Override
            public int compare(Employee employee1, Employee employee2) {
                return employee1.getName().compareTo(employee2.getName());
            }
        });
        showEmployees(employees);

        // Let's repeat the sorting process with a lambda
        printSeparator();
        employees = new ArrayList<>();
        employees.add(john);
        employees.add(tim);
        employees.add(jack);
        employees.add(snow);

        Collections.sort(employees, (Employee employee1, Employee employee2) ->
                employee1.getName().compareTo(employee2.getName()));
        showEmployees(employees);

        /* The lambda expression can be further simplified. If the compiler can infer the parameter types, we don't have
        to include them. In this case, the compiler can infer from the first parameter (Employees) that the object
        compared will both be of type Employee: */
        Collections.sort(employees, (employee1, employee2) ->
                employee1.getName().compareTo(employee2.getName()));

        /* What if the method of the interface that we´re calling with the lambda has a return? This would be the code
        we intent to do, in the classic way: */
        printSeparator();
        String sillyString = doStringStuff(new UpperConcat() {
           @Override
           public String upperAndConcat(String s1, String s2) {
               return s1.toUpperCase() + s2.toUpperCase();
           }
        },
        employees.get(0).getName(), employees.get(1).getName());
        System.out.println(sillyString);

        /* Now with lambda, apparently it could be like this:
        UpperConcat uc = (String s1, String s2) -> (return (s1.toUpperCase() + s2.toUpperCase());
        But that would give a compile error. If the only method of the interface has a return statement
        it is inferred by the compiler */
        printSeparator();
        UpperConcat uc = (s1, s2) -> s1.toUpperCase() + s2.toUpperCase();
        sillyString = doStringStuff(uc, employees.get(1).getName(), employees.get(2).getName());
        System.out.println(sillyString);

        /* We can assign a lambda to a variable and user it later on. If
        the lambda contains more than one statement, the return keyword has to be used.*/
        printSeparator();
        UpperConcat ucVariable = (s1, s2) -> {
            String result = s1.toUpperCase() + s2.toUpperCase();
            return result;
        };
        sillyString = doStringStuff(ucVariable, employees.get(2).getName(), employees.get(3).getName());
        System.out.println(sillyString);

        // It is also possible to call lambdas from a different class:
        printSeparator();
        AnotherClass anotherClass = new AnotherClass();
        String s = anotherClass.doSomething();
        System.out.println(s);

        // Example of what happens if trying to access an external variable from within an anonymous class:
        printSeparator();
        s = anotherClass.doSomethingAnonymousClassExternalVariable();
        System.out.println(s);

        // With lambdas, external variables can be managed as lambdas are part of the scope of the class that contains it
        // (see the code in AnotherClass):
        printSeparator();
        s = anotherClass.doSomethingLambdaExternalVariable();
        System.out.println(s);
    }

    public final static String doStringStuff(UpperConcat uc, String s1, String s2) {
        return uc.upperAndConcat(s1, s2);
    }

    private static void showEmployees(List<Employee> employees) {
        for(Employee employee : employees) {System.out.println(employee.getName());}
    }

    private static void printSeparator(){System.out.println("\n*************************************\n");}
}