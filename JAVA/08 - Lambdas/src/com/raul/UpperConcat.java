package com.raul;

@FunctionalInterface
public interface UpperConcat {
    String upperAndConcat(String s1, String s2);
}
