package com.raul;

public class WhatIsLambda {
    public static void main(String[] args) {
        /* The only code that matters here is the println. The rest of the code is just to instantiate an object and
        implement the single method in the runnable interface */
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Printing from the runnable(Main)");
            }
        }).start();

        // Wouldn't it be nice if we could just pass the println statement to the Thread constructor and avoid writing
        // all the extra code? :) Welcome the lambda expression:
        new Thread(() -> System.out.println("Printing from the runnable(lambda)")).start();

        /* All lambdas have three parts:
        1- Argument list: empty in this case (the brackets before the arrow)
        2- Arrow token: self explanatory
        3- Body: the code that we want to run (the println in this case)
        */

        /* Because with lambdas the compiler has to discern the executable method of the class we´re instantiating,
        lambdas only work for interfaces that contain only one method to be implemented. That kind of interface
        is also known as "functional interface" (@FunctionalInterface) */

        // If the lambda has to have more than one line, we can surround the body with curly braces {}
        new Thread(() -> {
            System.out.println("Multi line body for lambda.");
            System.out.println("other line");
            System.out.println("yet another one");
        }).start();

    }
}