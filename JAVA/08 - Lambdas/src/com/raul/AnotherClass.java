package com.raul;

public class AnotherClass {
    public String doSomething() {

        /*No lambda version:
        System.out.println("The another class class's name is: " + getClass().getSimpleName());
        return StartingExamples.doStringStuff(new UpperConcat() {
            @Override
            public String upperAndConcat(String s1, String s2) {
                //This should return an empty name as this is the implementation of an anonymous class
                System.out.println("The anonymous class class's name is: " + getClass().getSimpleName());
                return s1.toUpperCase() + s2.toUpperCase();
                }
            }, "String1", "String2");
        } */

        UpperConcat uc = (s1, s2) -> {
            /* Contrary to the previous case (no lambda version) this message actually prints a class name.
            This mean that lambdas are nested blocks of code and have the same scope as a nested block.
            In other words: lambdas are not classes but part of the class where they´re implemented. */
            System.out.println("The lambda expression's class is " + getClass().getSimpleName());
            String result = s1.toUpperCase() + s2.toUpperCase();
            return result;
        };

        System.out.println("The AnotherClass class´s name is " + getClass().getSimpleName());
        return StartingExamples.doStringStuff(uc, "String1", "String2");
    }

    public String doSomethingAnonymousClassExternalVariable() {
        /* Code within a nested block (inside {}) can reference variables defined within the enclosing block.
        That is, the block of code that contains it (the doSomethingElse method body, in this case). However, if
        the variable is accessed from an anonymous class, it has to be instantiated as final. Why? Because the local
        variable doesn't belong to the anonymous class instance. What actually happens is that, when the instance is
        constructed, the variable is replaced with whatever was the value it contained. */
        final int i = 0;
        {
            UpperConcat uc = new UpperConcat() {
                @Override
                public String upperAndConcat(String s1, String s2) {
                    System.out.println("i (within anonymous class) = " + i);
                    return s1.toUpperCase() + s2.toUpperCase();
                }
            };
            System.out.println("The AnotherClass class´s name is " + getClass().getSimpleName());
            return StartingExamples.doStringStuff(uc, "String1", "String2");
        }
    }

    public String doSomethingLambdaExternalVariable() {
        /* No need to final keyword in the external variable, as the lambda is NOT an anonymous class but a nested block
        and actually belongs to the scope of the class that contains it. However, because lambdas may not be immediately
        evaluated, any variables used within them must be final. The external variables are managed AS if they were final,
        so they can be accessed but not modified (a "i++" sentence inside the lambda would give a compilation error) */
        int i = 0;
        UpperConcat uc = (s1, s2) -> {
            System.out.println("The lambda expression's class is " + getClass().getSimpleName());
            System.out.println("i in the lambda expression = " + i);
            String result = s1.toUpperCase() + s2.toUpperCase();
            return result;
        };
        System.out.println("The AnotherClass class´s name is " + getClass().getSimpleName());
        return StartingExamples.doStringStuff(uc, "String1", "String2");
    }
}
