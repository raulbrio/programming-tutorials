package com.raul;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        Message message = new Message();
        (new Thread(new Writer(message))).start();
        (new Thread(new Reader(message))).start();
    }
}

class Message {
    private String message;
    private boolean empty = true; // True when there's no message to read

    /* Write and read are synchronized methods as we don't want a message to be written and read at the same time (For
    instance one thread writing on it while another thread is reading it) */
    public synchronized String read() {
        while(empty) {
            try {
                /* The wait() method is used to make the current thread release the lock and make it wait until another
                thread calls on the notify() or the notifyAll() method for the object or for a specific amount of time */
                wait();
            } catch (InterruptedException e){
                e.getStackTrace();
            }
        }
        empty = true;

        /* The notify() method is used to wake up a single thread that was paused by using the wait() method.
        One of the threads that are waiting on the object’s lock is selected arbitrarily and awakened using this method.

        Similarly, the notifyAll() method wakes up all the threads that were paused by using the wait() method
        and are waiting on the object’s lock.*/
        notifyAll();
        return message;
    }

    public synchronized void write(String message) {
        while(!empty) { //Don't write if there is a message yet to be read
            try {
                wait();
            } catch (InterruptedException e){
                e.getStackTrace();
            }
        }
        empty = false;
        this.message = message;
        notifyAll();
    }
}

// Producer class in a producer-consumer model
class Writer implements Runnable {
    private Message message;

    public Writer(Message message) {
        this.message = message;
    }

    public void run() {
        String messages[] = {
            "Humpty Dumpty sat on a wall",
            "Humpty Dumpty had a great fall",
            "All the king's horses and all the king's men",
            "Couldn't put Humpty together again"
        };

        Random random = new Random();

        for (int i=0; i<messages.length; i++) {
            message.write(messages[i]);
            try {
                Thread.sleep(random.nextInt(2000));
            } catch (InterruptedException e) {
                e.getStackTrace();
            }
        }
        message.write("Finished");
    }
}

class Reader implements Runnable {
    private Message message;

    public Reader(Message message) {
        this.message = message;
    }

    public void run() {
        Random random = new Random();
        for (String latestMessage = message.read(); !latestMessage.equals("Finished"); latestMessage = message.read()) {
            System.out.println(latestMessage);
            try {
                Thread.sleep(random.nextInt(2000));
            } catch (InterruptedException e) {
                e.getStackTrace();
            }
        }
    }
}