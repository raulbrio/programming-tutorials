package com.raul;

public class Main {

    public static void main(String[] args) {
        threadInterference();

        waitForMe(1);
        System.out.println("\n=====================\n");

        threadWithSeparateInstances();

        waitForMe(1);
        System.out.println("\n=====================\n");

        threadSynchronization();
    }

    public static void threadInterference() {
        // The counter variable in the Countdown class for loop is not initialized in the loop itself:
        //      for(int i=10; i > 0; i--) {
        // We're using a instance variable instead. Object instances are stored in the heap, and all threads share the same memory heap
        // Because this instance is being shared by all the threads, they're updating over the same for loop and the
        // output of the program gets crazy. Both threads are decrementing the same variable and messing each other's outcome.
        //
        // This is known as THREAD INTERFERENCE
        System.out.println("\nThread interference output:");
        Countdown countdown = new Countdown();
        CountdownThread t1 = new CountdownThread(countdown);
        t1.setName("Thread 1");
        CountdownThread t2 = new CountdownThread(countdown);
        t1.setName("Thread 2");
        t1.start();
        t2.start();
    }

    public static void threadWithSeparateInstances() {
        // Creating separated instances, each thread keeps it's own version of the instance variables (They have their own memory heap)
        // This prevents thread interference
        System.out.println("\nThreads using individual instances");
        Countdown countdownA = new Countdown();
        Countdown countdownB = new Countdown();
        CountdownThread t1 = new CountdownThread(countdownA);
        CountdownThread t2 = new CountdownThread(countdownB);
        t1.setName("Thread 1");
        t2.setName("Thread 2");
        t1.start();
        t2.start();
    }

    public static void threadSynchronization() {
        // This execution is the same used in "threadInterference()" but the countdown method is synchronized.
        // This suspends any thread trying to access until the thread running the method exits it.
        System.out.println("\nThread synchronization output:");
        SynchronizedCountdown syncCountdown = new SynchronizedCountdown();
        SyncCountdownThread t1 = new SyncCountdownThread(syncCountdown);
        t1.setName("Thread 1");
        SyncCountdownThread t2 = new SyncCountdownThread(syncCountdown);
        t1.setName("Thread 2");
        t1.start();
        t2.start();
    }

    public static void waitForMe(int sleepSeconds) {
        try {
            Thread.sleep(sleepSeconds * 1000);
        } catch (InterruptedException e) {
            e.getStackTrace();
        }
    }
}

class Countdown {
    private int i;
    public void doCountdown() {
        String color;

        switch (Thread.currentThread().getName()) {
            case "Thread 1":
                color = ThreadColor.ANSI_CYAN;
                break;

            case "Thread 2":
                color = ThreadColor.ANSI_PURPLE;
                break;

            default:
                color = ThreadColor.ANSI_GREEN;
                break;
        }

        for(i=10; i > 0; i--) {
            System.out.println(color + Thread.currentThread().getName() + ": i=" + i);
        }
    }
}

class CountdownThread extends Thread {
    private Countdown threadCountdown;

    public CountdownThread(Countdown countdown) {
        this.threadCountdown = countdown;
    }

    public void run() {
        threadCountdown.doCountdown();
    }
}

class SynchronizedCountdown {
//    private int i;
//    public synchronized void doCountdown() {
//        String color;
//
//        switch (Thread.currentThread().getName()) {
//            case "Thread 1":
//                color = ThreadColor.ANSI_CYAN;
//                break;
//
//            case "Thread 2":
//                color = ThreadColor.ANSI_PURPLE;
//                break;
//
//            default:
//                color = ThreadColor.ANSI_GREEN;
//                break;
//        }
//
//        for(i=10; i > 0; i--) {
//            System.out.println(color + Thread.currentThread().getName() + ": i=" + i);
//        }
//    }

    // Instead of synchronizing the whole method, the precise code that needs synchronization can be synchronized instead
    private int i;
    public void doCountdown() {
        String color;

        switch (Thread.currentThread().getName()) {
            case "Thread 1":
                color = ThreadColor.ANSI_CYAN;
                break;

            case "Thread 2":
                color = ThreadColor.ANSI_PURPLE;
                break;

            default:
                color = ThreadColor.ANSI_GREEN;
                break;
        }

        // When using a local object variable (color in this case) the reference is stored in the thread stack but the
        // object values are stored in the heap. Since the threads will create their own copy of the object, the object
        // references will be different. There won't be any interference even though the object values are in the heap.
        // The thread stack will only contain primitive values, object references and function references.
        // So this wouldn't work as the color variable will be part of the thread memory instead of the memory heap.
        // However objects are stored in the memory heap, thus using "this" will work for synchronizing

//        synchronized(color) {
//            for(i=10; i > 0; i--) {
//                System.out.println(color + Thread.currentThread().getName() + ": i=" + i);
//            }
//        }

        // Synchronization has been reduced to the for loop, as its always good to synchronize as less code as possible
        // Threads shouldn't be blocked or suspended unnecessarily.
        synchronized(this) {
            for(i=10; i > 0; i--) {
                System.out.println(color + Thread.currentThread().getName() + ": i=" + i);
            }
        }
    }
}

class SyncCountdownThread extends Thread {
    private SynchronizedCountdown threadCountdown;

    public SyncCountdownThread(SynchronizedCountdown countdown) {
        this.threadCountdown = countdown;
    }

    public void run() {
        threadCountdown.doCountdown();
    }
}