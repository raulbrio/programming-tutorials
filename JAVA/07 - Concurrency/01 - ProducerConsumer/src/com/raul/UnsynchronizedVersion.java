package com.raul;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.raul.UnsynchronizedVersion.EOF;

public class UnsynchronizedVersion {
    public static final String EOF = "EOF";

    public static void main(String[] args) {
        // This code, without any synchronization actions, is hugely prone to error

        List<String> buffer = new ArrayList<>();
        MyUnsynchronizedProducer producer = new MyUnsynchronizedProducer(buffer, ThreadColor.ANSI_YELLOW);
        MyUnsynchronizedConsumer consumer1 = new MyUnsynchronizedConsumer(buffer, ThreadColor.ANSI_PURPLE);
        MyUnsynchronizedConsumer consumer2 = new MyUnsynchronizedConsumer(buffer, ThreadColor.ANSI_CYAN);

        new Thread(producer).start();
        new Thread(consumer1).start();
        new Thread(consumer2).start();
    }
}

class MyUnsynchronizedProducer implements Runnable {
    private List<String> buffer;
    private String color;

    public MyUnsynchronizedProducer(List<String> buffer, String color) {
        this.buffer = buffer;
        this.color = color;
    }

    public void run() {
        Random random = new Random();
        String[] nums = {"1","2","3","4","5"};

        for (String num:nums) {
            try {
                System.out.println(color + "Adding... " + num);
                buffer.add(num);

                Thread.sleep(random.nextInt(1000));
            } catch (InterruptedException e) {
                System.out.println("Producer was interrupted");
            }
        }
        System.out.println(color + "Adding EOF and exiting...");

        buffer.add("EOF");
    }
}

class MyUnsynchronizedConsumer implements Runnable {
    private List<String> buffer;
    private String color;

    public MyUnsynchronizedConsumer(List<String> buffer, String color) {
        this.buffer = buffer;
        this.color = color;
    }

    public void run() {
        while(true) {
            if (buffer.isEmpty()) {
                continue;
            }
            if (buffer.get(0).equalsIgnoreCase(EOF)) {
                System.out.println(color + "Exiting...");
                break;
            } else {
                System.out.println(color + "Removed " + buffer.remove(0));
            }
        }
    }
}