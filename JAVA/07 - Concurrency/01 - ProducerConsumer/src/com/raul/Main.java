package com.raul;

public class Main {
    /* Creation order of the samples:
    * 1 - UnsynchronizedVersion
    * 2 - SynchronizedVersion
    * 3 - ReentrantVersion
    * 4 - TryFinallyVersion
    * 5 - ThreadPoolVersion
    * 6 - ArrayBlockingQueueVersion
    * More details in https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/locks/Lock.html
    */
}