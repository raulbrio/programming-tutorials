package com.raul;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

import static com.raul.ReentrantVersion.EOF;

public class ReentrantVersion {
    public static final String EOF = "EOF";

    public static void main(String[] args) {

        List<String> buffer = new ArrayList<>();
        /* Reentrantlock, as its name suggests, is a lock with reentrancy safety (it can be accessed or "entered" by multiple processes safely)
        If the thread is already holding a reentrant lock, when it reaches the code that requires the same lock, it can continue executing.
        It doesn't have to obtain the lock again.

        So it is pretty much used the same way as the synchronized() block, but it doesn't just convert the block
        into synchronized code. Insterad, it runs with true concurrency (threads can access that code simultaneously)
        but dealing with interference by itself */

        /* Here methods lock() (to acquire the lock) and unlock() (to release the lock) are used surrounding the code that needs to be synchronized.
        If the unlock() method is missing, the thread can block the execution of the application.
        If it's in a loop, lock() methods can accumulate indefinitely until the Java process crashes */

        ReentrantLock bufferLock = new ReentrantLock();
        MyReentrantProducer producer = new MyReentrantProducer(buffer, ThreadColor.ANSI_YELLOW, bufferLock);
        MyReentrantConsumer consumer1 = new MyReentrantConsumer(buffer, ThreadColor.ANSI_PURPLE, bufferLock);
        MyReentrantConsumer consumer2 = new MyReentrantConsumer(buffer, ThreadColor.ANSI_CYAN, bufferLock);

        new Thread(producer).start();
        new Thread(consumer1).start();
        new Thread(consumer2).start();
    }
}

class MyReentrantProducer implements Runnable {
    private List<String> buffer;
    private String color;
    private ReentrantLock bufferLock;

    public MyReentrantProducer(List<String> buffer, String color, ReentrantLock bufferLock) {
        this.buffer = buffer;
        this.color = color;
        this.bufferLock = bufferLock;
    }

    public void run() {
        Random random = new Random();
        String[] nums = {"1","2","3","4","5"};

        for (String num:nums) {
            try {
                System.out.println(color + "Adding... " + num);
                bufferLock.lock();
                buffer.add(num);
                bufferLock.unlock();

                Thread.sleep(random.nextInt(1000));
            } catch (InterruptedException e) {
                System.out.println("Producer was interrupted");
            }
        }
        System.out.println(color + "Adding EOF and exiting...");
        bufferLock.lock();
        buffer.add("EOF");
        bufferLock.unlock();
    }
}

class MyReentrantConsumer implements Runnable {
    private List<String> buffer;
    private String color;
    private ReentrantLock bufferLock;

    public MyReentrantConsumer(List<String> buffer, String color, ReentrantLock bufferLock) {
        this.buffer = buffer;
        this.color = color;
        this.bufferLock = bufferLock;
    }

    public void run() {
        while(true) {
            bufferLock.lock();
            if (buffer.isEmpty()) {
                bufferLock.unlock();
                continue;
            }
            if (buffer.get(0).equalsIgnoreCase(EOF)) {
                System.out.println(color + "Exiting...");
                bufferLock.unlock();
                break;
            } else {
                System.out.println(color + "Removed " + buffer.remove(0));
            }
            bufferLock.unlock();
        }
    }
}