package com.raul;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;
import java.util.concurrent.locks.ReentrantLock;

import static com.raul.TryFinallyVersion.EOF;

public class ThreadPoolVersion {
    public static final String EOF = "EOF";

    public static void main(String[] args) {
        List<String> buffer = new ArrayList<>();
        ReentrantLock bufferLock = new ReentrantLock();

        // Create a pool with the required threads, 3 in this case (one for the producer and one for each consumer)
        ExecutorService executorService = Executors.newFixedThreadPool(5); //Change number of threads to see differences in ExecutorService

        MyThreadPoolProducer producer = new MyThreadPoolProducer(buffer, ThreadColor.ANSI_YELLOW, bufferLock);
        MyThreadPoolConsumer consumer1 = new MyThreadPoolConsumer(buffer, ThreadColor.ANSI_PURPLE, bufferLock);
        MyThreadPoolConsumer consumer2 = new MyThreadPoolConsumer(buffer, ThreadColor.ANSI_CYAN, bufferLock);

        executorService.execute(producer);
        executorService.execute(consumer1);
        executorService.execute(consumer2);

        // A "Future" represents the result of an asynchronous computation.
        // The result can only be retrieved using method "get" when the computation has completed, blocking if
        // necessary until it is ready.
        Future<String> future = executorService.submit(new Callable<>() {
            @Override
            public String call() throws Exception {
                System.out.println(ThreadColor.ANSI_WHITE + "I'm being printed for the Callable class");
                return "This is the callable result";
            }
        });

        try {
            System.out.println(future.get());
        } catch (ExecutionException e) {
            System.out.println("Something went wrong");
        } catch (InterruptedException e) {
            System.out.println("Thread running the task was interrupted");
        }

        // When shutdown is called the service will wait for any executing task to terminate.
        // If shutdown is not used, the application execution will never finalize
        executorService.shutdown();

        //More info at: https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/ExecutorService.html
    }
}

class MyThreadPoolProducer implements Runnable {
    private List<String> buffer;
    private String color;
    private ReentrantLock bufferLock;

    public MyThreadPoolProducer(List<String> buffer, String color, ReentrantLock bufferLock) {
        this.buffer = buffer;
        this.color = color;
        this.bufferLock = bufferLock;
    }

    public void run() {
        Random random = new Random();
        String[] nums = {"1","2","3","4","5"};

        for (String num:nums) {
            try {
                System.out.println(color + "Adding... " + num);
                bufferLock.lock();
                try {
                    buffer.add(num);
                } finally {
                    bufferLock.unlock();
                }
                Thread.sleep(random.nextInt(1000));
            } catch (InterruptedException e) {
                System.out.println("Producer was interrupted");
            }
        }
        System.out.println(color + "Adding EOF and exiting...");
        bufferLock.lock();
        try {
            buffer.add("EOF");
        } finally {
            bufferLock.unlock();
        }
    }
}

class MyThreadPoolConsumer implements Runnable {
    private List<String> buffer;
    private String color;
    private ReentrantLock bufferLock;

    public MyThreadPoolConsumer(List<String> buffer, String color, ReentrantLock bufferLock) {
        this.buffer = buffer;
        this.color = color;
        this.bufferLock = bufferLock;
    }

    public void run() {
        int counter = 0;

        while(true) {
            // Here, tryLock() is used instead of lock()
            // tryLock() acquires the lock only if it is free at the time of invocation.
            if (bufferLock.tryLock()) {
                try {
                    if (buffer.isEmpty()) {
                        continue;
                    }
                    //Added just to show how many iterations it can wait until it's free (spoiler: TOO MANY, multi threading is a crazy shit)
                    System.out.println(color + "The counter = " + counter);
                    counter = 0;

                    if (buffer.get(0).equalsIgnoreCase(EOF)) {
                        System.out.println(color + "Exiting...");
                        break;
                    } else {
                        System.out.println(color + "Removed " + buffer.remove(0));
                    }
                } finally {
                    bufferLock.unlock();
                }
            } else {
                counter ++;
            }
        }
    }
}