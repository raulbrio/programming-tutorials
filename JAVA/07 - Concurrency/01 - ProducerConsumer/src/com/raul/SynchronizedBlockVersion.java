package com.raul;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.raul.SynchronizedBlockVersion.EOF;

public class SynchronizedBlockVersion {
    public static final String EOF = "EOF";

    public static void main(String[] args) {
        // synchronized() blocks work as non-multithreading code. When the execution enters a synchronized() block, it stays
        // there until it's done, and it can't be interrupted by another thread

        // The most sensible part of this code to the lack of synchronization is the ArrayList, as it is unsynchronized.
        // To solve that, let's put all code responsible of the ArrayList (used as a buffer) in synchronized() blocks

        // The synchronized block works pretty well, but it has lots of performance drawbacks (it basically destroys the
        // advantages in performance of multi-threading)

        List<String> buffer = new ArrayList<>();
        MySynchronizedProducer producer = new MySynchronizedProducer(buffer, ThreadColor.ANSI_YELLOW);
        MySynchronizedConsumer consumer1 = new MySynchronizedConsumer(buffer, ThreadColor.ANSI_PURPLE);
        MySynchronizedConsumer consumer2 = new MySynchronizedConsumer(buffer, ThreadColor.ANSI_CYAN);

        new Thread(producer).start();
        new Thread(consumer1).start();
        new Thread(consumer2).start();
    }
}

class MySynchronizedProducer implements Runnable {
    private List<String> buffer;
    private String color;

    public MySynchronizedProducer(List<String> buffer, String color) {
        this.buffer = buffer;
        this.color = color;
    }

    public void run() {
        Random random = new Random();
        String[] nums = {"1","2","3","4","5"};

        for (String num:nums) {
            try {
                System.out.println(color + "Adding... " + num);

                synchronized (buffer) {
                    buffer.add(num);
                }

                Thread.sleep(random.nextInt(1000));
            } catch (InterruptedException e) {
                System.out.println("Producer was interrupted");
            }
        }
        System.out.println(color + "Adding EOF and exiting...");

        synchronized (buffer) {
            buffer.add("EOF");
        }
    }
}

class MySynchronizedConsumer implements Runnable {
    private List<String> buffer;
    private String color;

    public MySynchronizedConsumer(List<String> buffer, String color) {
        this.buffer = buffer;
        this.color = color;
    }

    public void run() {
        while(true) {

            synchronized (buffer) {
                if (buffer.isEmpty()) {
                    continue;
                }
                if (buffer.get(0).equalsIgnoreCase(EOF)) {
                    System.out.println(color + "Exiting...");
                    break;
                } else {
                    System.out.println(color + "Removed " + buffer.remove(0));
                }
            }
        }
    }
}