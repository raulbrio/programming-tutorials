package com.raul;

import java.util.Random;
import java.util.concurrent.*;

import static com.raul.TryFinallyVersion.EOF;

public class ArrayBlockingQueueVersion {
    public static final String EOF = "EOF";

    public static void main(String[] args) {
        // Concurrent Collections:
        // Standard collections such as Set, HashSet, Map, HashMap and so on, have static methods to make those collections
        // thread safe. For instance with a HashSet:
        //
        // final Set< String > strings =
        // Collections.synchronizedSet( new HashSet< String >() );
        //
        // Let's use a thread safe type or array: the ArrayBlockingQueue. With this we no longer need the
        // bufferLock methods (tryLock, lock, unlock, etc). Also, they are MUCH more efficient.
        // We also have different methods to interact with the array:
        //
        // .put() instead of .add()
        // .peek() instead of .get()
        // .take() instead of .remove()
        // Another thread-safe collections: ConcurrentHashMap<K,V> ConcurrentLinkedQueue<E> ConcurrentSkipListMap<K,V>

        ArrayBlockingQueue<String> buffer = new ArrayBlockingQueue<String>(6); //As any other array, the size has to be defined

        // Create a pool with the required threads, 3 in this case (one for the producer and one for each consumer)
        ExecutorService executorService = Executors.newFixedThreadPool(5); //Change number of threads to see differences in ExecutorService

        MyArrayBlockingQueueProducer producer = new MyArrayBlockingQueueProducer(buffer, ThreadColor.ANSI_YELLOW);
        MyArrayBlockingQueueConsumer consumer1 = new MyArrayBlockingQueueConsumer(buffer, ThreadColor.ANSI_PURPLE);
        MyArrayBlockingQueueConsumer consumer2 = new MyArrayBlockingQueueConsumer(buffer, ThreadColor.ANSI_CYAN);

        executorService.execute(producer);
        executorService.execute(consumer1);
        executorService.execute(consumer2);

        // A "Future" represents the result of an asynchronous computation.
        // The result can only be retrieved using method "get" when the computation has completed, blocking if
        // necessary until it is ready.
        Future<String> future = executorService.submit(new Callable<>() {
            @Override
            public String call() throws Exception {
                System.out.println(ThreadColor.ANSI_WHITE + "I'm being printed for the Callable class");
                return "This is the callable result";
            }
        });

        try {
            System.out.println(future.get());
        } catch (ExecutionException e) {
            System.out.println("Something went wrong");
        } catch (InterruptedException e) {
            System.out.println("Thread running the task was interrupted");
        }

        // When shutdown is called the service will wait for any executing task to terminate.
        // If shutdown is not used, the application execution will never finalize
        executorService.shutdown();

        //More info at: https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/ExecutorService.html
    }
}

class MyArrayBlockingQueueProducer implements Runnable {
    private ArrayBlockingQueue<String> buffer;
    private String color;

    public MyArrayBlockingQueueProducer(ArrayBlockingQueue<String> buffer, String color) {
        this.buffer = buffer;
        this.color = color;
    }

    public void run() {
        Random random = new Random();
        String[] nums = {"1","2","3","4","5"};

        for (String num:nums) {
            try {
                System.out.println(color + "Adding... " + num);
                buffer.put(num); //We use .put() instead of .add()

                Thread.sleep(random.nextInt(1000));
            } catch (InterruptedException e) {
                System.out.println("Producer was interrupted");
            }
        }
        System.out.println(color + "Adding EOF and exiting...");
        try {
            buffer.put("EOF");
        } catch(InterruptedException ie) {
            System.out.println(ie.getMessage());
        }
    }
}

class MyArrayBlockingQueueConsumer implements Runnable {
    private ArrayBlockingQueue<String> buffer;
    private String color;

    public MyArrayBlockingQueueConsumer(ArrayBlockingQueue<String> buffer, String color) {
        this.buffer = buffer;
        this.color = color;
    }

    public void run() {

        while(true) { // We no longer need the traylock method as we're not using bufferLock
            synchronized (buffer) {
                try {
                    if (buffer.isEmpty()) {
                        continue;
                    }
                    if (buffer.peek().equalsIgnoreCase(EOF)) { // We use .peek() instead of .get()
                        System.out.println(color + "Exiting...");
                        break;
                    } else {
                        System.out.println(color + "Removed " + buffer.take()); // We use .take() instead of .remove()
                    }
                } catch (InterruptedException ie) {
                    System.out.println(ie.getMessage());
                }
            }
        }
    }
}