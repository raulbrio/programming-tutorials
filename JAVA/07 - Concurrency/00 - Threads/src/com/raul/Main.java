package com.raul;

import static com.raul.ThreadColor.*;

public class Main {

    public static void main(String[] args) {
        /* Threads can be created either implementing Runnable or extending Thread
         The Thread class itself implements Runnable with an empty implementation of run() method.

        **** WHEN TO USE THREAD OR RUNNABLE ****
        Extending class from Thread is limited as Java doesn't allow multiple inheritance (You can only extend from one class)
        Inheritance is meant for extending the functionality of the parent, but the thread is created only to provide the
        implementation of the run() method. Extending just for this reason is not a good practice.

        So in general it's always recommended to use Runnable object. This method allows the class to extend from any other class
        and makes possible to use lambda expressions with Runnable */

        System.out.println(ANSI_PURPLE + "Hello from the main thread");
        Thread anotherThread = new AnotherThread();
        anotherThread.setName("== Another Thread ==");
        anotherThread.start();

        // Threads instances have to be called with the method start() and NOT with run(), as threads are actually called
        // by the JVM and not the user. If we call he method run() the code won't be run in a new thread but in the
        // thread where it was called
        Thread badlyCalledThread = new SimpleThread();
        badlyCalledThread.setName("== I wont appear :( ==");
        badlyCalledThread.run(); // This should output "Hello from main"

        // Thread can be created with an anonymous class (a lambda can be used the same way)
        new Thread() {
            public void run() {
                System.out.println(ANSI_GREEN + "Hello from the anonymous class thread");
            }
        }.start();

        // Thread can be created by subclassing the Thread class
        Thread myRunnableThread = new Thread(new MyRunnable());
        myRunnableThread.start();

        // Thread can be also created with an anonymous class implementing runnable and passing an instance of it
        // to the thread constructor
        Thread myRunnableThreadAbstract = new Thread(new MyRunnable() {
            @Override
            public void run() {
                // super.run();
                System.out.println(ANSI_CYAN + "Hello from the anonymous class's implementation of run()");
                try {
                    // Join waits for the called thread (anotherThread) until it terminates, then continues it's work
                    // If waiting time overpass the milliseconds specified, it will continue execution
                    anotherThread.join(5000);
                    System.out.println(ANSI_RED + "AnotherThread terminated (or timed out), so I'm running again");
                } catch (InterruptedException e) {
                    System.out.println(ANSI_RED + "I couldn't wait after all, I was interrupted");
                }
            }
        });
        myRunnableThreadAbstract.start();
        // anotherThread.interrupt(); // This will trigger the message "Another thread woke me up" from AnotherThread

        System.out.println(ANSI_PURPLE+"Hello again from the main thread");

    }
}
