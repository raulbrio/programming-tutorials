package com.raul;

import static com.raul.ThreadColor.*;

public class MyRunnable implements Runnable{
    // Runnable interface is the primary template for any object that is intended to be executed by a thread.
    // It defines a single method run(), which is meant to contain the code that is executed by the thread.
    // Any class whose instance needs to be executed by a thread should implement the Runnable interface.

    @Override
    public void run() {
        System.out.println(ANSI_RED + "Hello from my runnable implementation of run");
    }
}
