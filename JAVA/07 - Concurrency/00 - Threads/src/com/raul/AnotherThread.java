package com.raul;

import static com.raul.ThreadColor.*;

public class AnotherThread extends Thread {
    // You can create a new thread simply by extending your class from Thread and overriding it’s run() method.
    @Override
    public void run() {
        // The run() method contains the code that is executed inside the new thread.
        // Once a thread is created, you can start it by calling the start() method.
        System.out.println(ANSI_BLUE+"Hello from " + currentThread().getName());

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            System.out.println(ANSI_RED + "Another thread woke me up");
            return;
        }

        System.out.println(ANSI_BLUE + "Three seconds have passed and I'm awake");
    }
}
