package com.raul;

public class DeadlockDemo {

    /* Deadlock is a part of multithreading in Java. Suppose there is a thread waiting for a lock of an object which
    is currently acquired by another thread and the second thread is waiting for a lock on an object that is currently
    acquired by the first thread. Deadlock appears in this situation. This case is known as deadlock because both the
    threads are waiting for each other to release the object’s lock. */

    public static void main(String[] args) {
        final String name1 = "John";
        final String name2 = "Alex";

        Thread t1 = new Thread() {
            public void run() {
                synchronized (name1) {
                    System.out.println("Thread1 locked with name1...");
                    try {
                        Thread.sleep(1000); // <------
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    synchronized (name2) {
                        System.out.println("Thread1 locked with name2...");
                    }
                }
            }
        };
        Thread t2 = new Thread() {
            public void run() {
                synchronized (name2) {
                    System.out.println("Thread2 locked with name2...");
                    try {
                        Thread.sleep(1000); // <--------
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    synchronized (name1) {
                        System.out.println("Thread2 locked with name1...");
                    }
                }
            }
        };
        t1.start();
        t2.start();
    }
}