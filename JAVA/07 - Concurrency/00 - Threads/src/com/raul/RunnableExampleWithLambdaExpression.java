package com.raul;

public class RunnableExampleWithLambdaExpression {
    public static void main(String[] args) {
        // Same example as RunnableExampleWithAnonymousClass but using Lambda instead
        System.out.println("Inside : " + Thread.currentThread().getName());

        System.out.println("Creating Runnable...");
        Runnable runnable = () -> {
            System.out.println("Inside : " + Thread.currentThread().getName());
        };

        System.out.println("Creating Thread...");
        Thread thread = new Thread(runnable);

        System.out.println("Starting Thread...");
        thread.start();

    }
}
