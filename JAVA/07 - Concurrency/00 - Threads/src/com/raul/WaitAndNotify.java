package com.raul;

/*
WAIT METHOD:
 The wait() method is used to make the current thread release the lock and make it wait until another thread calls
 on the notify() method or the notifyAll() method for the object or for a specific amount of time. It can only be invoked
 from the synchronized method because the current thread should own this object’s monitor.
 If not, the method with throw an exception.

 Methods:
 public final void wait():
    This method causes the current thread to wait until the object is notified.

 public final void wait(long timeout):
    This method causes the current thread to wait for a particular amount of time.

NOTIFY METHOD:
 The notify() method is used to wake up a single thread that was paused by using the wait() method.
 One of the threads that are waiting on the object’s lock is selected arbitrarily and awakened using this method.

 The notifyAll() method wakes up all the threads that were paused by using the wait() method and are waiting on the
 object’s lock. The following is the syntax of this method:
*/

class Customer {
    int amount = 3000;
    synchronized void withdraw(int amount) {
        System.out.println("Going to withdraw: ");
        if (this.amount < amount)
            System.out.println("Less balance wait for deposit..");

        try {
            wait(); // <----
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.amount -= amount;
        System.out.println("Withdraw completed...");
    }
    synchronized void deposit(int amount) {
        System.out.println("Going to deposit...");
        this.amount += amount;
        System.out.println("Deposit is completed...");
        notify(); // <----
    }
}
public class WaitAndNotify {

    public static void main(String[] args) {

        Customer c = new Customer();
        new Thread() {
            public void run() {

                c.withdraw(5000);
            }
        }.start();

        new Thread() {
            public void run() {

                c.deposit(5000);
            }
        }.start();
    }
}