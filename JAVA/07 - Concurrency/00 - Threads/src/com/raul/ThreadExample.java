package com.raul;

public class ThreadExample extends Thread{
    // run() method contains the code that is executed by the thread.
    // Once a thread is created, you can start it by calling the start() method.
    @Override
    public void run() {
        System.out.println("Inside : " + Thread.currentThread().getName());
    }

    public static void main(String[] args) {

        // Thread.currentThread() returns a reference to the thread that is currently executing.
        // In this example, thread’s getName() method is used to print the name of the current thread.
        System.out.println("Inside : " + Thread.currentThread().getName());
        //Every thread has a name. you can create a thread with a custom name using Thread(String name) constructor.
        // If no name is specified then a new name is automatically chosen for the thread.

        System.out.println("Creating thread...");
        Thread thread = new ThreadExample();

        System.out.println("Starting thread...");
        thread.start();
    }
}
