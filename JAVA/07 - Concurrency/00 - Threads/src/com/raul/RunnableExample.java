package com.raul;

public class RunnableExample implements Runnable {
    public static void main(String[] args) {
        // For creating a new thread, create an instance of the class that implements Runnable interface
        // and then pass that instance to Thread(Runnable target) constructor
        System.out.println("Inside : " + Thread.currentThread().getName());

        System.out.println("Creating Runnable...");
        Runnable runnable = new RunnableExample();

        System.out.println("Creating Thread...");
        Thread thread = new Thread(runnable);

        System.out.println("Starting Thread...");
        thread.start();
    }

    @Override
    public void run() {
        System.out.println("Inside : " + Thread.currentThread().getName());
    }
}
