package com.raul;

import static com.raul.ThreadColor.ANSI_YELLOW;

public class SimpleThread extends Thread {
    @Override
    public void run() {
        System.out.println(ANSI_YELLOW+"Hello from " + currentThread().getName());
    }
}
