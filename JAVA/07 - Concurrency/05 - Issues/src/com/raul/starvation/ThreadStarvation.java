package com.raul.starvation;

import com.raul.ThreadColor;

public class ThreadStarvation {

    private static Object lock = new Object();

    public static void main(String[] args) {
        // Starvation occurs when a thread has to wait for too long to be attended. This may be caused mainly by three factors:
        // 1- Too many threads
        // 2- Other threads are too "greedy"
        // 3- Bad configuration of semaphores, priorities and bad thread planning and ordering in general

        Thread t1 = new Thread(new Worker(ThreadColor.ANSI_RED), "Priority 1");
        Thread t2 = new Thread(new Worker(ThreadColor.ANSI_BLACK), "Priority 10");
        Thread t3 = new Thread(new Worker(ThreadColor.ANSI_BLUE), "Priority 6");
        Thread t4 = new Thread(new Worker(ThreadColor.ANSI_CYAN), "Priority 2");
        Thread t5 = new Thread(new Worker(ThreadColor.ANSI_GREEN), "Priority 8");

        // A way to solve (partially) this issue is to use thread priority. Keep in mind that the "priority"
        // in a thread is only a "suggestion". It's not binding anything at all.
        // The OS chooses the execution order every time the code is executed.
        t1.setPriority(1);
        t2.setPriority(10);
        t3.setPriority(6);
        t4.setPriority(2);
        t5.setPriority(8);

        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
    }

    private static class Worker implements Runnable {
        private int runCount = 1;
        private String threadColor;

        public Worker(String threadColor) {
            this.threadColor = threadColor;
        }

        @Override
        public void run() {
            for (int i = 0; i<100; i++){
                synchronized (lock) {
                    System.out.format(threadColor + "%s: runCount = %d\n", Thread.currentThread().getName(), runCount++);
                    // execute critical section of code
                }
            }
        }
    }
}
