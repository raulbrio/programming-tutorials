package com.raul.deadlocks;

public class CallSyncMethodFromOtherObjectThread {
    public static void main(String[] args) {
        final PolitePerson jane = new PolitePerson("Jane");
        final PolitePerson john = new PolitePerson("John");

        // As long as this is part of the same thread there's no conflict
//        jane.sayHello(john);
//        john.sayHello(jane);

        // But if the direct call are commented and this is launched as separated threads, a deadlock appears

        new Thread(new Runnable() {
            @Override
            public void run() {
                jane.sayHello(john);
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                john.sayHello(jane);
            }
        }).start();

        /*Deadlock description:
        1 - Thread1 acquires the lock on the jane object and enters the sayHello() method. It prints to the console, then suspends.
        2 - Thread2 acquires the lock on the john object and enters the sayHello() method. It prints to the console, then suspends.
        3 - Thread1 runs again and wants to say hello back to the john object. It tries to call the sayHelloBack() method using
        the john object that was passed into the sayHello() method, but Thread2 is holding the john lock, so Thread1 suspends.
        4 - Thread2 runs again and wants to say hello back to the jane object. It tries to call the sayHelloBack() method using
        the jane object that was passed into the sayHello() method, but Thread2 is holding the jane lock, so Thread1 suspends.
        */
    }

    static class PolitePerson {
        private final String name;

        public PolitePerson(String name){
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public synchronized void sayHello(PolitePerson person){
            System.out.format("%s: %s" + " has said hello to me!%n", this.name, person.getName());
            person.sayHelloBack(this); // This is the key of the Deadlock: The sayHelloBack method called is not from "this" object but from the other (person) one.
        }

        public synchronized void sayHelloBack(PolitePerson person){
            System.out.format("%s: %s" + " has said hello back to me!%n", this.name, person.getName());
        }
    }
}
