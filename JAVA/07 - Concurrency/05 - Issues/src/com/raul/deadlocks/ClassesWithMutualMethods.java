package com.raul.deadlocks;


public class ClassesWithMutualMethods {
    // Here is an example of deadlock when two classes contain synchronized methods
    // and each class calls a method in the other class

    /*
    Let's recall that when a thread is running an object's synchronized method, no other thread can run a
    synchronized method using the same object until the first thread exits the method it's running.
     */

    public static void main(String[] args) {
        Data data = new Data();
        Display display = new Display();
        data.setDisplay(display);
        display.setData(data);

        new Thread(){
            public void run() {
            data.updateData();
            }
        }.start();

        new Thread(){
            public void run() {
                display.updateDisplay();
            }
        }.start();


        /* We have a deadlock:
         Both threads are trying to get the same two locks in a different order. Thread1 wants gets the Data lock,
         then wants the Display lock.
         Thread2 gets the Display lock and then wants de Data lock.
        */
    }

    private static class Data {
        private Display display;

        public void setDisplay(Display display){
            this.display = display;
        }

        public synchronized void updateData() {
            System.out.println("Updating data...");
            display.dataChanged();
        }

        public synchronized Object getData() {
            return new Object();
        }
    }

    public  static class Display {
        private Data data;

        public void setData(Data data) {
            this.data = data;
        }

        public synchronized void dataChanged() {
            System.out.println("I'm doing something because the data changed...");
        }

        public synchronized void updateDisplay() {
            System.out.println("Updating display...");
            Object o = data.getData();
        }
    }
}
