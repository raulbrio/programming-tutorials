package com.raul.deadlocks;

public class SimpleDeadlock {

    // This is a basic example of a thread deadlock
    public static Object lock1 = new Object();
    public static Object lock2 = new Object();

    public static void main(String[] args) {
        new Thread1().start();
        new Thread2().start();
    }

    private static class Thread1 extends Thread {
        public void run() {
            synchronized (lock1) {
                System.out.println("Thread1: Has lock1");
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ie) {
                    System.out.println(ie.getMessage());
                }
                System.out.println("Thread1: Waiting for lock2");
                synchronized (lock2) {
                    System.out.println("Thread1: Has lock1 and lock2");
                }
                System.out.println("Thread1: Released lock2");
            }
            System.out.println("Thread1: Released lock1. Exiting...");
        }
    }

    private static class Thread2 extends Thread {
        public void run() {
            synchronized (lock2) {
                System.out.println("Thread2: Has lock2");
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ie) {
                    System.out.println(ie.getMessage());
                }
                System.out.println("Thread2: Waiting for lock1");
                synchronized (lock1) {
                    System.out.println("Thread2: Has lock2 and lock1");
                }
                System.out.println("Thread2: Released lock1");
            }
            System.out.println("Thread2: Released lock2. Exiting...");
        }
    }
}
