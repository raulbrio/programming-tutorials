package com.raul.fairlockandlivelock;

import com.raul.ThreadColor;

import java.util.concurrent.locks.ReentrantLock;

public class FairLock {

    // A fair lock guarantees the thread ordering "FIFS" (first come first serve). This is NOT FIFO as the fair lock means that
    // there is fairness in ACQUIRING the lock, not fairness in thread's scheduling.

    //Let's say the thread that gets the lock takes long. The rest of threads may take long to start even though the "fair" lock is set up

    private static ReentrantLock lock = new ReentrantLock(true); //This boolean sets up if this lock will be a fair lock

    public static void main(String[] args) {


        Thread t1 = new Thread(new Worker(ThreadColor.ANSI_RED), "Priority 1");
        Thread t2 = new Thread(new Worker(ThreadColor.ANSI_BLACK), "Priority 10");
        Thread t3 = new Thread(new Worker(ThreadColor.ANSI_BLUE), "Priority 6");
        Thread t4 = new Thread(new Worker(ThreadColor.ANSI_CYAN), "Priority 2");
        Thread t5 = new Thread(new Worker(ThreadColor.ANSI_GREEN), "Priority 8");


        t1.setPriority(1);
        t2.setPriority(10);
        t3.setPriority(6);
        t4.setPriority(2);
        t5.setPriority(8);

        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
    }

    private static class Worker implements Runnable {
        private int runCount = 1;
        private String threadColor;

        public Worker(String threadColor) {
            this.threadColor = threadColor;
        }

        @Override
        public void run() {
            for (int i = 0; i<100; i++){
                synchronized (lock) {
                    System.out.format(threadColor + "%s: runCount = %d\n", Thread.currentThread().getName(), runCount++);
                    // execute critical section of code
                }
            }
        }
    }
}
