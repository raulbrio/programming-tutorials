package com.timbuchalka._static;

public class Main {

    public static int multiplier = 7;
    public int nonStatic = 0;

    public static void main(String[] args) {
        // Static == non dependant on the instance (object) of a class
	    StaticTest firstInstance = new StaticTest("1st Instance");
        System.out.println(firstInstance.getName() + " is instance number " + StaticTest.getNumInstances());

        StaticTest secondInstance = new StaticTest("2nd instance");
        System.out.println(secondInstance.getName() + " is instance number " + StaticTest.getNumInstances());

        StaticTest thirdInstance = new StaticTest("3rd instance");
        System.out.println(thirdInstance.getName() + " is instance number " + StaticTest.getNumInstances());

        // No object is needed to work with statics
        int answer = multiply(6);
        System.out.println("The answer is " + answer);
        System.out.println("Multiplier is " + multiplier);

        // Following is wrong, as non static variables and methods can't be called from a static context (in this case
        // the main method is static) if they're not instantiated (part of an object)
        // System.out.println(nonStatic);

    }

    public static int multiply(int number) {
        return number * multiplier;
    }
}
