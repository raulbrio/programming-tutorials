package candyBag;

public class Chocolat {
    private String brand;
    public String getBrand() {
        return brand;
    }
    public void setBrand(String brand) {
        this.brand = brand;
    }
    public Chocolat(String brand) {
        super();
        this.brand = brand;
    }
}
