package candyBag;

import java.util.ArrayList;
import java.util.Iterator;

// Using generic "T" because the type of object that the new object will receive (the items we will add to the bag) are
// not defined until we create the bag and input stuff into it
public class BagWithGenerics<T> implements Iterable<T> {
    private ArrayList<T> list = new ArrayList();
    private int top; //Variable for the maximum size of the bag
    public BagWithGenerics(int top) {
        super();
        this.top = top;
    }
    public void add(T o) {
        if (list.size()<= top) {
            list.add(o);
        }else {
            throw new RuntimeException("No room for more");
        }
    }
    public Iterator<T> iterator() {
        return list.iterator();
    }
}
