package candyBag;

import java.util.ArrayList;
import java.util.Iterator;

public class Bag implements Iterable{
    private ArrayList list = new ArrayList();
    private int top; //Variable for the maximum size of the bag
    public Bag(int top) {
        super();
        this.top = top;
    }
    public void add(Object o) {
        if (list.size()<= top) {
            list.add(o);
        }else {
            throw new RuntimeException("No room for more");
        }
    }
    public Iterator iterator() {
        return list.iterator();
    }
}
