package candyBag;

public class Main {
    public static void main(String[] args) {
        Bag bag = new Bag(5);
        Chocolat c1 = new Chocolat("Milka");
        Chocolat c2 = new Chocolat("Nestle");
        Candy ca1 = new Candy("Lollipop");
        Candy ca2 = new Candy("Popsicle");

        bag.add(c1);
        bag.add(c2);
        bag.add(ca1);
        bag.add(ca2);

        //Checking object type with "instanceof"
        for (Object o: bag) {
            if (o instanceof Chocolat) {
                Chocolat chocolat = (Chocolat) o;
                System.out.println(chocolat.getBrand());
            } else {
                Candy candy = (Candy)o;
                System.out.println(candy.getName());
            }
        }

        System.out.println("===================================");

        // When creating the object we choose the type of object we set into
        BagWithGenerics<Chocolat> bagWithGenerics = new BagWithGenerics<>(5);
        Chocolat c3 = new Chocolat("Lindt");
        Chocolat c4 = new Chocolat("Trappa");

        bagWithGenerics.add(c3);
        bagWithGenerics.add(c4);

        // Because the type is already defined the object can be iterated without checking as it happened in the first one
        for (Chocolat chocolat:bagWithGenerics) {
            System.out.println(chocolat.getBrand());
        }
    }
}
