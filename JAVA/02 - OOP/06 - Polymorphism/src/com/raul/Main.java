package com.raul;

class Movie {
    private String name;

    public Movie(String name) {
        this.name = name;
    }

    //This is the method that will be overridden
    public String plot(){
        return "No plot here";
    }

    public String getName() {
        return name;
    }
}

class ReadyPlayerOne extends Movie {
    public ReadyPlayerOne() {
        super("Ready Player One");
    }

    @Override
    public String plot() {
        return "A nerd tries to become millionaire";
    }
}

class BladeRunner extends Movie {
    public BladeRunner() {
        super("Blade Runner");
    }

    @Override
    public String plot() {
        return "Disturbing robots fuck it all up";
    }
}

class TheMatrix extends Movie {
    public TheMatrix() {
        super( "The Matrix");
    }

    @Override
    public String plot() {
        return "People dressed like drag queens discover they're battery cells";
    }
}

class Interstellar extends Movie {
    public Interstellar() {
        super("Interstellar");
    }

    @Override
    public String plot() {
        return "Humanity prefer to live miserably on space than to fix earth";
    }
}

class Forgettable extends Movie {
    public Forgettable() {
        super("Forgettable");
    }

    // No plot overridden method for this child
}

public class Main {

    public static void main(String[] args) {
        for (int i = 1; i < 10; i++){
            Movie movie = randomMovie();
            System.out.println("Movie #" + i + ":" + movie.getName() + "\n" +
                    // Polymorphism is used here; movie.plot() is the overridden method.
                    // When any random movie (all of them are children of Movie) is called by the
                    // method "plot", the overridden method will be used if that class has it. If not,
                    // the "plot" method from the parent will be used instead.
                    "Plot> " + movie.plot() + "\n");
        }
    }

    public static Movie randomMovie() {
        int randomNumber = (int) (Math.random() * 5) + 1;
        System.out.println("Random number generated is " + randomNumber);

        switch (randomNumber) {
            case 1:
                return new ReadyPlayerOne();
            case 2:
                return new BladeRunner();
            case 3:
                return new TheMatrix();
            case 4:
                return new Interstellar();
            case 5:
                return new Forgettable();
            default:
                return null;
        }
    }
}
