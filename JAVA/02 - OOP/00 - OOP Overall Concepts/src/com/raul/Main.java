package com.raul;

public class Main {

    public static void main(String[] args) {
	/* Application for hamburger shop */

	// Base hamburger
	Hamburger hamburger = new Hamburger("Basic", "Sausage" , 3.56, "White");
	hamburger.addHamburgerAddition1("Tomato", 0.27);
	hamburger.addHamburgerAddition2("Lettuce", 0.75);
	hamburger.addHamburgerAddition3("Cheese", 1.13);
	System.out.println("Total Burger price: " + hamburger.itemizeHamburger());

	System.out.println("======================================");

	// Healthy hamburger
	HealthyBurger healthyBurger = new HealthyBurger("Bacon", 5.67);
	healthyBurger.addHamburgerAddition1("Egg", 1.43);
	healthyBurger.addHamburgerAddition3("Pickles", 0.82);
	healthyBurger.addHamburgerAddition4("Fried onion", 0.65);
	healthyBurger.addHealthAddition2("Toffu", 1.11);
	System.out.println("Total Healthy Burger price: " + healthyBurger.itemizeHamburger());

	System.out.println("======================================");

	DeluxeBurger deluxeBurger = new DeluxeBurger();
	deluxeBurger.addHamburgerAddition1("This shouldnt be allowed", 20.34);
	System.out.println("Total Deluxe Burger price: " + deluxeBurger.itemizeHamburger());
    }
}
