package com.raul;

public class Printer {
    private String model;
    private int tonerLevel;
    private int pagesPrinted = 0;
    private boolean duplexCapable;

    public Printer(String model){
        this(model, 0, false);
    }
    public Printer(String model, int tonerLevel, boolean duplexCapable) {
        this.model = model;
        if (tonerLevel <= 100 && tonerLevel >= 0){
            this.tonerLevel = tonerLevel;
        } else {
            this.tonerLevel = 0;
        }
        this.duplexCapable = duplexCapable;
    }

    public void fillToner(int toner){
        if ((toner > 0 && toner <= 100) && (toner + this.tonerLevel <= 100)){
            System.out.println("Filling toner");
            this.tonerLevel = this.tonerLevel + toner;
            this.currentTonerLevel();
        } else {
            System.out.println("Toner amount incorrect");
            this.currentTonerLevel();
        }
    }

    public void fillToner(){
        this.tonerLevel = 100;
        System.out.println("Toner filled to maximum.");
        this.currentTonerLevel();
    }

    public void printJob(){
        System.out.println("Attempting to print a single page");
        if ((this.duplexCapable == true && this.tonerLevel < 2) || (this.duplexCapable == false && this.tonerLevel < 1)){
            System.out.println("Not enough. Cant print");
            this.currentTonerLevel();
        } else {
           this.actualPrint();
            System.out.println("Page printed.");
            this.currentTonerLevel();
            this.totalPagesPrinted();
        }
    }

    public void printJob(int pages){
        System.out.println("Attempting to print " + pages + "page(s)");
        if ((this.duplexCapable == true && pages * 2 > this.tonerLevel) || (this.duplexCapable == false && pages > this.tonerLevel)) {
            System.out.println("Not enough toner for this job. Please refill toner or print less pages");
            this.currentTonerLevel();
        } else {
            for (int i = 0; i < pages; i++){
                this.actualPrint();
                System.out.println("Printing page " + (i + 1) + " of " + pages);
                this.currentTonerLevel();
            }
            this.totalPagesPrinted();
        }
    }

    private void actualPrint(){
        if (this.duplexCapable == true){
            System.out.println("Printing double sided!");
            this.tonerLevel = this.tonerLevel - 2;
        } else {
            this.tonerLevel --;
        }
        this.pagesPrinted ++;
    }

    public void currentTonerLevel(){
        System.out.println("Current toner level: " + this.tonerLevel + "%");
    }

    public void totalPagesPrinted(){
        System.out.println("Pages printed by " + this.model + ": " + this.pagesPrinted);
    }
    public String getModel() {
        return model;
    }
}
