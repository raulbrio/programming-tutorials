package com.raul;

public class Main {

    public static void main(String[] args) {

        //First printer created with small constructor and perform small method "printJob"
        Printer printer1 = new Printer("Canon BJC-200");
        System.out.println(printer1.getModel());
        printer1.currentTonerLevel();
        printer1.fillToner();
        printer1.printJob();
        printer1.printJob(100);

        System.out.println("===========================");

        //Second printer created with full constructor and perform complex "printJob"
        Printer printer2 = new Printer("Heweld Packard HP-100", 80, false);
        System.out.println(printer2.getModel());
       printer2.currentTonerLevel();
        printer2.printJob(50);
        printer2.fillToner(80);

        System.out.println("===========================");

        //Third printer is duplex capable
        Printer printer3 = new Printer("Epson Stylus EPS-2200", 50, true);
        System.out.println(printer3.getModel());
        printer3.currentTonerLevel();
        printer3.printJob();
    }
}
