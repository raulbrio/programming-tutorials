package com.raul;

//Example of composition against inheritance
public class Main {

    public static void main(String[] args) {

    	//To send the objet Dimensions to Case, the object Dimensions can be created first
        Dimensions dimensions = new Dimensions(200, 20, 5);
	    Case theCase = new Case("220B", "Dell", "240", dimensions);

	    //To send the object Resolution to Monitor, the object Resolution can be created INTO the Monitor declaration itself
	    Monitor theMonitor = new Monitor("27\" Beast", "Acer", 27, new Resolution(2540, 1440));

	    Motherboard theMotherboard = new Motherboard("BJ-200", "Asus", 4, 6, "v2.44");

	    PC thePC = new PC(theCase, theMonitor, theMotherboard);

	    thePC.powerUp();
    }
}
