package com.raul;

//This is the master object
public class PC {
    // The properties of the master object are other objects. The relation between them is not IS-A but either HAS-A,
    // that's why heritage is not being used, but composition (a monitor IS NOT a computer, but a computer HAS A monitor)
    private Case theCase;
    private Monitor monitor;
    private Motherboard motherboard;

    public PC(Case theCase, Monitor monitor, Motherboard motherboard) {
        this.theCase = theCase;
        this.monitor = monitor;
        this.motherboard = motherboard;
    }

    public void powerUp() {
        theCase.pressPowerButton();
        drawLogo();
    }

    private void drawLogo() {
        //Fancy graphics
        monitor.drawPixelat(1200, 50, "yellow");
    }
}
