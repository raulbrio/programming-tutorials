package com.timbuchalka;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

	    Gearbox mcLaren = new Gearbox(6);

	    // ** If inner class is public **
	    // Correct way of declaring an object of an inner class
        // Gearbox.Gear first = mcLaren.new Gear(1, 12.3);
        // Incorrect ways of declaring an object of an inner class
        // Gearbox.Gear second = new Gearbox.Gear(2, 15.4);
        // Gearbox.Gear third = new mcLaren.Gear(3, 17.8);
        // System.out.println(first.driveSpeed(1000));

        mcLaren.changeGear(2);
        mcLaren.operateClutch(true);
        mcLaren.changeGear(1);
        System.out.println("Car wheels running at " + mcLaren.wheelSpeed(3000));
        mcLaren.operateClutch(false);
        System.out.println("Car wheels running at " + mcLaren.wheelSpeed(3000));
    }
}
