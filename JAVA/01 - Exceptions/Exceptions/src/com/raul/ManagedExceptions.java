package com.raul;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class ManagedExceptions {
    public static void main(String[] args) {
        try {
            int result = divide();
            System.out.println("Division result: " + result);
        } catch (ArithmeticException | NoSuchElementException e) {
            System.out.println(e.toString());
            System.out.println("Unable to perform division, autopilot shutting down");
        }

    }

    private static int divide(){
        int x;
        int y;
        //This two try-catch methods can (and must) be combined in a multi catch exception
//        try {
//            x = getIntEAFP();
//            y = getIntEAFP();
//        } catch (NoSuchElementException e) {
//            throw new ArithmeticException("No suitable input");
//        }
//        System.out.println("x is " + x + ", y is " + y);
//        try {
//            return x / y;
//        } catch(ArithmeticException e) {
//            throw new ArithmeticException("attempt to divide by zero");
//        }

        //Exceptions can be managed and catch in the calling side (main), so they're not needed in the method
//        try {
//            x = getInt();
//            y = getInt();
//            System.out.println("x is " + x + ", y is " + y);
//            return x / y;
//        } catch (NoSuchElementException e) {
//            throw new NoSuchElementException("No suitable input");
//        } catch (ArithmeticException e) {
//            throw new ArithmeticException("attempt to divide by zero");
//        }

        x = getInt();
        y = getInt();
        System.out.println("x is " + x + ", y is " + y);
        return x / y;
    }

    private static int getInt() {
        Scanner s = new Scanner(System.in);
        System.out.println("Please enter an integer");
        while(true) {
            try {
                return s.nextInt();
            } catch (InputMismatchException e) {
                // go round again, read past the end of line in the input first
                s.nextLine();
                System.out.println("Please enter a number using only the digits 0 to 9");
            }
        }
    }
}
