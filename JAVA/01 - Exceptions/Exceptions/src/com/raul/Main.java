package com.raul;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        /*
        There are two ways to deal with exceptions:
        - Look before you leap(BYL): make sure the data being used is the correct and
        expected so it won't cause errors (For instance: check if variable that should
        be an integer is not a string, or null, or anything else

        - Better ask for forgiveness than for permission(EAFP): Just encapsulate everything in a
        try/catch expression and capture any possible error as an exception.
         */

//	    int x = 98;
//	    int y = 0;
//	      // Both of these methods prevent dividing by zero
//        System.out.println(divideBYL(x, y));
//        System.out.println(divideEAFP(x, y));
//        // This method doesn't deal with exception so it causes a runtime exception
//        System.out.println(divide(x, y));

        //int x = getIntBYL();
        int x = getIntEAFP();
        System.out.println("x is " + x);
    }

    private static int getInt() {
        Scanner s = new Scanner(System.in);
        return s.nextInt();
    }

    private static int getIntBYL() {
        Scanner s = new Scanner(System.in);
        boolean isValid = true;
        System.out.println("Please enter an integer");
        String input = s.next();
        for (int i = 0; i < input.length(); i++) {
            if (!Character.isDigit(input.charAt(i))) {
                isValid = false;
                break;
            }
        }
        if (isValid) {
            return Integer.parseInt(input);
        }
        return 0;
    }

    private static int getIntEAFP() {
        Scanner s = new Scanner(System.in);
        System.out.println("Please enter an integer");
        try {
            return s.nextInt();
        } catch (InputMismatchException e) {
            return 0;
        }
    }

    private static int divideBYL(int x, int y) { //look before you leap
        if (y != 0){
            return x / y;
        } else {
            return 0;
        }
    }

    private static int divideEAFP(int x, int y) {  //Ask for forgiveness than permission
        try {
            return x / y;
        } catch(ArithmeticException e) {
            return 0;
        }
    }

    private static int divide(int x, int y) {
        return x / y;
    }
}
