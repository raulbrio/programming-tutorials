package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Button btn = new Button();
        btn.setText("Say 'Hello World'");
        btn.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println("Hello World!");
            }
        });

        //Through the FXM file elements can be created and described
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));

        //The following lines are the same element described in the FXM file, but created directly into the code
        //GridPane root = new GridPane();
        //root.setAlignment(Pos.CENTER);
        //root.setVgap(10);
        //root.setHgap(10);

        //Label greeting = new Label("Welcome stranger");
        //greeting.setTextFill(Color.GREEN);
        //greeting.setFont(Font.font("Times New Roman", FontWeight.BOLD, 70));
        //root.getChildren().add(greeting);
        //root.getChildren().add(btn);

        Scene scene = new Scene(root, 700, 250);

        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
