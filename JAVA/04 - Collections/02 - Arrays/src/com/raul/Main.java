package com.raul;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int regularVariable = 25;
        //Accessing regular variable
        System.out.println(regularVariable);

        //New int array for 10 elements
        int[] intArray = new int[10];
        //Writing the array
        intArray[4] = 50; //Accessing fifth position as arrays start counting on zero
        //Accesing the array
        System.out.println(intArray[4]);

        //Arrays can be declared with assigned values. Array for 5 elements with values
        int[] assignedArray = {23, 45, 53, 56, 12};
        System.out.println(assignedArray[2]); //Accessing third position in the array

        //An array can also be filled and read using loops
        int[] arrayFromLoop = new int[12];
        for (int i = 0; i < arrayFromLoop.length; i++) {
            arrayFromLoop[i] = i + i * 3;
        }

        //An array can be sent to a method or anywhere else
        printArray(arrayFromLoop);

        //Using an array to obtain an average of a list of numbers
        int[] myIntegers = getIntegers(5);
        printArray(myIntegers);
        System.out.println("Average of the values introduced is " + getAverage(myIntegers));

        //Arrays can be easily sorted using parallel arrays
        int[] sortedArray = sortArray(myIntegers);
        System.out.println("This is the array sorted in descending order:");
        for (int i=0; i<sortedArray.length; i++){
            System.out.println(sortedArray[i]);
        }
    }


    public static void printArray(int[] array){
        for (int i=0; i< array.length; i++){
            System.out.println("Element" + (i +1) + ", value is " + array[i]);
        }
    }

    private static int[] getIntegers(int number) {
        System.out.println("Enter " + number + " integer values.\r");
        int[] values = new int[number];

        for(int i=0; i<values.length; i++){
            values[i] = scanner.nextInt();
        }
        return values;
    }

    public static double getAverage(int[] array){
        int sum = 0;
        for(int i=0; i<array.length; i++){
            sum += array[i];
        }
        return (double) sum / (double) array.length; //Calculating average by the sum of values divided by the number of values
    }

    public static int[] sortArray(int[] array){
        int[] sortedArray = Arrays.copyOf(array, array.length);

        boolean flag = true;
        int temp;

        while(flag){
            flag=false;
            for (int i=0; i < sortedArray.length - 1; i++){
                if (sortedArray[i] < sortedArray[i+1]){
                    temp = sortedArray[i];
                    sortedArray[i] = sortedArray[i+1];
                    sortedArray[i+1] = temp;
                    flag = true;
                }
            }
        }
        return sortedArray;
    }
}
