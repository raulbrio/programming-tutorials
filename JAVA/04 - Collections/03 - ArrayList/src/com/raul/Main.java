package com.raul;

import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);
    private static GroceryList groceryList = new GroceryList();

    public static void main(String[] args) {
        boolean quit = false;
        int choice = 0;
        while (!quit){
            System.out.println("Enter your choice: ");
            printInstructions();
            choice = scanner.nextInt();
            scanner.nextLine();

            switch(choice){
                case 0:
                    printInstructions();
                    break;
                case 1:
                    groceryList.printAllItemsFromGroceryList();
                    System.out.println("Press enter to continue...");
                    scanner.nextLine();
                    break;
                case 2:
                    addItemToGroceryList();
                    break;
                case 3:
                    modifyItemsFromGroceryList();
                    break;
                case 4:
                    removeItemsFromGroceryList();
                    break;
                case 5:
                    searchItemFromGroceryList();
                    break;
                case 6:
                    showItemFromGroceryList();
                    break;
                case 7:
                    quit = true;
                    break;
                default:
                    break;
            }
        }
    }

    public static void printInstructions(){
        System.out.println("\n Press");
        System.out.println("\t 0 - Print choice options");
        System.out.println("\t 1 - Print list of grocery items");
        System.out.println("\t 2 - Add an item");
        System.out.println("\t 3 - Modify an item");
        System.out.println("\t 4 - Remove an item");
        System.out.println("\t 5 - Search for an item");
        System.out.println("\t 6 - Show an item of the list");
        System.out.println("\t 7 - Quit the application");
    }

    public static void addItemToGroceryList() {
        System.out.println("Enter grocery item");;
        groceryList.addItemToGroceryList(scanner.nextLine());
    }

    public static void modifyItemsFromGroceryList() {
        System.out.println("Enter item number");
        int itemNumber = scanner.nextInt();
        scanner.nextLine();
        System.out.println("Enter replacement item");
        String newItem = scanner.nextLine();
        groceryList.modifyItemsFromGroceryList(itemNumber-1, newItem);
    }

    public static void removeItemsFromGroceryList(){
        System.out.println("Enter item number");
        int itemNumber = scanner.nextInt();
        scanner.nextLine();
        groceryList.removeItemsFromGroceryList(itemNumber-1);
    }

    public static void searchItemFromGroceryList(){
        System.out.println("Enter item to search for");
        String searchItem = scanner.nextLine();
        String result = groceryList.findItemFromGroceryList(searchItem);
        if ( result != null){
            System.out.println("Found " + searchItem + " in the list in position " + result);
        } else {
            System.out.println(searchItem + " not found in the list");
        }
    }

    public static void showItemFromGroceryList(){
        System.out.println("Enter index of item");
        int searchItem = scanner.nextInt();
        groceryList.printItemFromGroceryList(searchItem);
    }
}
