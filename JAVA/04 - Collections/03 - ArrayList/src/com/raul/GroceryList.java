package com.raul;

import java.util.ArrayList;

public class GroceryList {
    // ArrayLists are dynamic arrays. They can hold objects and change size dynamically
    // The type of data the ArrayList will hold is not defined as with the rest of types and variables,
    // but inside <>. Comparison between a regular string array and an ArrayList of strings:

    private String[] myStrings;
    private ArrayList<String> myGroceryList = new ArrayList<String>();

    public void addItemToGroceryList(String item){
        //Elements are not added using a numbered index like in regular arrays, but using a method of the ArrayList class
        myGroceryList.add(item);
    }

    public void printAllItemsFromGroceryList(){
        //The size of the ArrayList is obtained with .size() method, and the desired element with .get() method
        System.out.println("Number of elements in the GroceryList: " + myGroceryList.size());
        for (int i = 0; i < myGroceryList.size(); i++) {
            System.out.println((i+1) + ", " + myGroceryList.get(i));
        }
    }

    public void printItemFromGroceryList(int position){
        System.out.println(myGroceryList.get(position));
    }

    public void modifyItemsFromGroceryList(int position, String newValue){
        //Items in the ArrayList can be modified with the method .set()
        myGroceryList.set(position, newValue);
        System.out.println("Item in position " + (position + 1) + "has now the value " + myGroceryList.get(position));
    }

    public void removeItemsFromGroceryList(int position) {
        //To remove an element in an ArrayList the method .remove() is used
        String theItem = myGroceryList.get(position);
        myGroceryList.remove(position);
    }

    public String findItemFromGroceryList(String searchItem){
        //The method contains() returns a boolean indicating whether the item exists in the ArrayList or not
        boolean exists = myGroceryList.contains(searchItem);
        //The method indexOf() returns the position of the item in the ArrayList (or -1 if it doesn't exists)
        int position = myGroceryList.indexOf(searchItem);
        if (position >=0) {
            return myGroceryList.get(position);
        } return null;
    }
}
