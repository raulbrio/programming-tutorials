package com.timbuchalka;

import com.sun.org.apache.xpath.internal.operations.Bool;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class HashSetIterator {
    public static void main(String[] args) {
        String[] keys = {"a", "b", "c", "d", "e"};
        String[] values = {"value a", "value b", "value c", "value d", "value e"};
        String[] stringsToCompare = {"thisToRemove a", "thisToRemove patata", "thisToRemove c", "thisToRemove cosica", "thisToRemove e"};
        Map<String, String> keyValueHashMap = new HashMap<>(); // New hashmap

        for (int i = 0; i < values.length; i++) { // Feeding the hashmap
            keyValueHashMap.put(keys[i], values[i]);
        }

        Iterator<String> it = keyValueHashMap.keySet().iterator();
        Boolean deleteEntry;

        while (it.hasNext()) {
            String key = it.next();
            deleteEntry = true;
            for (String myString : stringsToCompare) {
                myString = myString.replaceFirst("thisToRemove ", "");
                // If the key of the element in the hashmap is the same as any of the strings to compare, NOT delete that entry in the hashmap
                if (keyValueHashMap.get(key).equals(myString)) { // TODO: Find out why the fuck this comparison isn´t working
                    deleteEntry = false;
                }
            }
            if (deleteEntry) {
                it.remove();
            }
        }
        for (Map.Entry<String, String> entry : keyValueHashMap.entrySet()) {
            System.out.println("Key: " + entry.getKey());
            System.out.println("Value: " + entry.getValue());
        }

    }
}
