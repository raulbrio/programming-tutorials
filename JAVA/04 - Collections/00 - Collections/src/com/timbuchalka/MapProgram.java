package com.timbuchalka;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dev on 8/12/2015.
 */
public class MapProgram {

    public static void main(String[] args) {
        /*
        Hashmap is an array of key-value entries
        LinkedHashMap is the same as Hashmap, but ordered
         */
        Map<String, String> languages = new HashMap<>();
        if(languages.containsKey("Java")) {
            System.out.println("Java already exists");
        } else {
            languages.put("Java", "a compiled high level, object-oriented, platform independent language");
            System.out.println("Java added successfully");
        }

        languages.put("Python", "an interpreted, object-oriented, high-level programming language with dynamic semantics");
        languages.put("Algol", "an algorithmic language");
        System.out.println(languages.put("BASIC", "Beginners All Purposes Symbolic Instruction Code"));
        System.out.println(languages.put("Lisp", "Therein lies madness"));

        if(languages.containsKey("Java")) {
            System.out.println("Java is already in the map");
        } else {
            languages.put("Java", "this course is about Java");
        }

        System.out.println("================================================");

        // Remove method can be used directly
        // languages.remove("Lisp");

        //Remove can be used with the key/value pair, and will only remove the entry when both values coincide
        if(languages.remove("Algol", "A family of algorithmic languages")) {
            System.out.println("Algol removed");
        } else {
            System.out.println("Algol not removed, key/value pair not found");
        }

        // Replace method only replaces the value of the key if the original value is present. Work simillarly to remove but replacing instead
        if(languages.replace("Lisp", "Therein lies madness", "a functional programming language with imperative features")) {
            System.out.println("Lisp replaced");
        } else {
            System.out.println("Lisp was not replaced");
        }

        //This will print null as the replace method is not satisfied (That key/value pair doesn't exist)
        System.out.println(languages.replace("Scala", "this will not be added"));

        for(String key: languages.keySet()) {
            System.out.println(key + " : " + languages.get(key));
        }
    }
}
