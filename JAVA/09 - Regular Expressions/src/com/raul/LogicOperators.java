package com.raul;

import java.nio.channels.SocketChannel;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LogicOperators {
    public static void main(String[] args) {
        // AND:
        // Given the regular expression "abc", in reality it would be the same as:
        // "a" and "b" and "c", where "and" would be the same as "is followed by"

        // OR:
        // The operator "or" can be defined by pipe "|":
        // [H|h]arry = "Harry" or "harry"
        System.out.println("harry".replaceAll("[H|h]arry", "Larry"));
        System.out.println("Harry".replaceAll("[H|h]arry", "Larry"));

        // NOT:
        // The symbols ^ and ! are used as "not". Examples:
        // [^abc] = character by character not "a" or "b" or "c"
        // negative lookahead: a(?!b) = "a" not followed by "b"
        // negative lookbehind: (?<!a)b = "b" not preceded by "a"

        // Lets create an example: Match occurrences of character "t" not followed by a character "v"
        String tvTest = "tstvtkt";
        String tNotVRegExp = "t[^v]";

        // The last "t" in the string won't be counter as an occurrence because the regex used means:
        // "character "t" followed by any character that is not a "v"". The last "t" doesn't have a following character
        System.out.println(occurrenceCounter(tvTest, tNotVRegExp));

        // Using ! instead of ^ will return 3 results instead of 2, as this regex means "character t not followed by v"
        // also note that when using a "look ahead", the character in the look ahead doesn't appear in the match. We're
        // getting results of one character size instead of two.
        // In other words: They don't include the characters in they match in the match
        tNotVRegExp = "t(?!v)";
        System.out.println(occurrenceCounter(tvTest, tNotVRegExp));

        // Examples:
        //Phone number with format (123) 456-7890
        String phoneRegex = "^([\\(]{1}[0-9]{3}[\\)]{1}[ ]{1}[0-9]{3}[\\-]{1}[0-9]{4})$";
        String phone1 = "1234567890"; // Shouldn't match
        String phone2 = "(123) 456-7890"; // Match
        String phone3 = "555 243-5298"; // Shouldn't match
        String phone4 = "(334)573-5573"; // Shouldn't match

        System.out.println("Phone 1: = " + phone1.matches(phoneRegex));
        System.out.println("Phone 2: = " + phone2.matches(phoneRegex));
        System.out.println("Phone 3: = " + phone3.matches(phoneRegex));
        System.out.println("Phone 4: = " + phone4.matches(phoneRegex));

        //Visa number that starts with 4 (as all VISAs followed by at least 12 numbers, followed by the next new three
        // characters (which are put in a group in the regex)
        String visaRegex = "^4[0-9]{12}([0-9]{3})?$";
        String visa1 = "4444444444444"; // match
        String visa2 = "5444444444444"; // shouldn't match
        String visa3 = "4444444444444444"; // match
        String visa4 = "4444"; // shouldn't match

        System.out.println("Visa 1: = " + visa1.matches(visaRegex));
        System.out.println("Visa 2: = " + visa2.matches(visaRegex));
        System.out.println("Visa 3: = " + visa3.matches(visaRegex));
        System.out.println("Visa 4: = " + visa4.matches(visaRegex));
    }

    private static String occurrenceCounter(String testString, String regex){
        int count = 0;
        String message = "";
        Pattern tNotVPattern = Pattern.compile(regex);
        Matcher matcher = tNotVPattern.matcher(testString);
        while (matcher.find()) {
            count ++;
            message += ("Occurrence " + count + " : " + matcher.start() + " to " + matcher.end() + "\n");
        }
        return message;
    }
}

