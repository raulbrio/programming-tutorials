package com.raul;

public class Main {

    public static void main(String[] args) {

        String string = "I am a string. Yes I am";
        System.out.println(string);

        // Replace with regex
        String yourString = string.replaceAll("I" , "you");
        System.out.println(yourString);

        // Character classes
        String alphanumeric = "abcDeeeF12GhhoabcDeeeiiiijkl99z";
        // "." is a wildcard for any character)
        System.out.println(alphanumeric.replaceAll(".", "Y"));

        // "^" == beginning of the string. This is why the following only replaces the fist ocurrence even though it's a "replaceAll"
        System.out.println(alphanumeric.replaceAll("^abcDeee", "YYY"));

        // A repeated character can be replaced by a quantifier:
        System.out.println(alphanumeric.replaceAll("^abcDe{3}", "XXX"));

        // Quantifiers can specify the number of occurrences (like the previous one) or just ask for "one or n" occurrences (using "+"):
        System.out.println(alphanumeric.replaceAll("^abcDe+", "XXX"));

        // Similarly it can be asked for "zero or n" occurrences (using "*"):
        System.out.println(alphanumeric.replaceAll("^abcDe*", "XXX"));

        // matches returns true if the content of both strings is the same
        System.out.println(alphanumeric.matches("^hello")); // This is false
        System.out.println(alphanumeric.matches("^abcDeee")); // This is also false. The regex is true but the strings are not equal
        System.out.println(alphanumeric.matches("abcDeeeF12GhhoabcDeeeiiiijkl99z")); // This is true

        // "$" == beginning of the string (the opossite to "^")
        System.out.println(alphanumeric.replaceAll("ijkl99z$", "THE END"));

        // Brackets specifies various possible values, for instance replace the vowels "a", "e" and "o" with "%"
        System.out.println(alphanumeric.replaceAll("[aeo]", "%"));

        // Conditions can be used. For instance, replace vowels "a", "e" and "o" if they're followed by an "b" or "F"
        // note that it replaces both letters (the "a", "e", or "o" and the "b" or "F")
        System.out.println(alphanumeric.replaceAll("[aeo][bF]", "*REPLACEMENT*"));

        // Brackets can also be used to check letter case. For instance, the following will replace the name all occurrences
        // of the name "harry" and "Harry"
        System.out.println("Harry is a good man, harry likes programming".replaceAll("[Hh]arry", "Peter"));

        // Regex can have exceptions with "[^]". For instance, replace every character in a string except "e" and "G"
        System.out.println(alphanumeric.replaceAll("[^eG]", "!"));

        // Replace a range of characters (letters "e" to "j" and numbers 3 to 8)
        System.out.println(alphanumeric.replaceAll("[e-j3-9]", "?"));

        // Same expression but with both high and low case letters
        System.out.println(alphanumeric.replaceAll("[e-jE-J3-9]", "?"));

        // "(?i)" tells the regex to ignore case, so the following will give the same output as the previous replaceAll
        System.out.println(alphanumeric.replaceAll("(?i)[e-j3-9]", "?"));

        // Two different ways to replace all numbers:
        System.out.println(alphanumeric.replaceAll("[0-9]", "&"));
        System.out.println(alphanumeric.replaceAll("\\d", "&"));

        // Replace all alphabetic characters:
        System.out.println(alphanumeric.replaceAll("\\D", "X"));

        // Remove all blank spaces (spaces, tabs, newlines and so on)
        String hasWhiteSpaces = "I have blanks and\ta tab, and also a \nnewline";
        System.out.println(hasWhiteSpaces);
        System.out.println(hasWhiteSpaces.replaceAll("\\s", ""));

        // Replace all characters except blank spaces
        System.out.println(hasWhiteSpaces.replaceAll("\\S", "@"));

        // Surround word (characters beetween blank spaces) with string
        System.out.println(hasWhiteSpaces.replaceAll("\\b", "_"));

        // Rules in regex can be combined at will. For instance, replace all occurrences of "h",
        // followed by any number of "i", followed by at least one "j"
        String anotherAlphanumeric = "abcDeeeF12Ghhiiiijkl99z";
        System.out.println(anotherAlphanumeric.replaceAll("h+i*j", "#"));
    }
}
