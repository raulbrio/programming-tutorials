package com.raul;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MatcherAndPattern {
    public static void main(String[] args) {
        // Extended information about Matcher and Pattern:
        // https://docs.oracle.com/javase/8/docs/api/java/util/regex/Matcher.html

        // Some java libraries doesn't like strings as regex and use patterns instead. Patterns
        // can be created from strings anyway:
        StringBuilder htmlText = new StringBuilder("<h1>My heading</h1>");
        htmlText.append("<h2>Sub-heading</h2>");
        htmlText.append("<p>This is a paragraph about something </p>");
        htmlText.append("<p>This is another paragraph about something else</p>");
        htmlText.append("<h2>Summary</h2>");
        htmlText.append("<p>Here is the summary</p>");

        String h2Pattern = "<h2>";
        Pattern pattern = Pattern.compile(h2Pattern, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
        Matcher matcher = pattern.matcher(htmlText);

        // matches() method returns "true" if pattern meets the string (for instance if the pattern were "*.<h2>.*") and
        // "false" otherwise (even though the pattern appears in the string, as the find() method shows just below)
        System.out.println("Matcher matches() method: ");
        System.out.println(matcher.matches());

        // Matchers can be used only once. The Matcher has to be reset, otherwise the following matcher.find() method
        // won't be executed as the matcher has been already used with the matches() method
        matcher.reset();

        // We can get the start and end of each occurrence
        int count=0;
        while(matcher.find()) {
            count ++;
            System.out.println("Occurrence " + count + " : " + matcher.start() + " to " + matcher.end());
        }

        // We create groups with brackets ( and )
        String h2GroupPattern = "(<h2>.*</h2>)";
        Pattern groupPattern = Pattern.compile(h2GroupPattern);
        Matcher groupMatcher = groupPattern.matcher(htmlText);
        System.out.println(groupMatcher.matches());
        groupMatcher.reset();

        // Greedy quantifiers does't stop when they find the occurrence, they continue parsin the string to find out if there is any ocurrence
        // bigger, where the first one is contained inside the second, larger one. For instance, the following returns everything the first <h2>
        // of the string all the way to the final </h2>
        System.out.println("Greedy quantifier: ");
        while (groupMatcher.find()){
            System.out.println("Occurrence " + groupMatcher.group(1)); // We can access the groups (we only have one for this example)
        }

        // Reluctant or Lazy quantifiers determine each occurrence as soon as there is a match:
        groupMatcher.reset();
        String h2GroupPattern2 = "(<h2>.*?</h2>)"; // We can convert a greedy (default) quantifier to a lazy quantifier adding ? to the pattern wildcard
        Pattern groupPattern2 = Pattern.compile(h2GroupPattern2);
        Matcher groupMatcher2 = groupPattern2.matcher(htmlText);

        System.out.println("Lazy quantifier: ");
        while (groupMatcher2.find()){
            System.out.println("Occurrence " + groupMatcher2.group(1));
        }

        // Using more than one group:
        String h2TextGroups = "(<h2>)(.+?)(</h2>)";
        Pattern h2TextPattern = Pattern.compile(h2TextGroups);
        Matcher h2TextMatcher = h2TextPattern.matcher(htmlText);

        System.out.println("Using various matcher groups");
        while (h2TextMatcher.find()){
            // using more than one group we can extract, in this case, the text between the tags (group 2) and ommit the tags itself
            System.out.println("Occurrence: " + h2TextMatcher.group(2));
        }
    }
}
