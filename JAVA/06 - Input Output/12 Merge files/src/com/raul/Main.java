package com.raul;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main {

    private static String destFile = "";

    public static void main(String[] args) throws Exception {

        String fileDirectory = "resources\\";
        File dir = new File(fileDirectory);


        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy");
        Date date = new Date();
        String dateString = formatter.format(date);
        destFile = dateString + "destination.txt";

        PrintWriter pw = new PrintWriter(fileDirectory + destFile);

        String[] fileNames = dir.list();

        for (String fileName : fileNames) {
            if (fileName.equals(destFile) || fileName.equals("readme.txt")) {
                continue;
            }
            System.out.println("Reading from " + fileName);
            File f = new File(dir, fileName);
            BufferedReader br = new BufferedReader(new FileReader(f));
            String line = br.readLine();
            while (line != null) {
                pw.println(line);
                line = br.readLine();
            }
            br.close();
            pw.flush();
        }
        System.out.println("Reading from all files in directory " + dir.getName() + " Completed");
        cleanDirectory(dir);
    }

    private static void cleanDirectory(File dir) {
        for (File file : dir.listFiles()) {
            if (!file.getName().equalsIgnoreCase("readme.txt") && !file.getName().equalsIgnoreCase(destFile)) {
                if (file.delete()) {
                    System.out.println("deleted " + file.getName());
                }
            }
        }
        System.out.println("Temporary .avro files deleted");
    }
}