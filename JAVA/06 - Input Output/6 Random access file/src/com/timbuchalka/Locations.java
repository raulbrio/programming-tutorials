package com.timbuchalka;

import java.io.*;
import java.util.*;

public class Locations implements Map<Integer, Location> {
    private static Map<Integer, Location> locations = new LinkedHashMap<Integer, Location>();
    private static Map<Integer, IndexRecord> index = new LinkedHashMap<>();
    private static RandomAccessFile ra;

    public static void main(String[] args) throws IOException {
        /* Structure of the random access file used here:
        1- The first 4 bytes (bytes 0-3) will contain the number of locations
        2- The next 4 bytes (bytes 4-7) will contain the start offset of the locations section, this is, where the
        index ends and the records start
        3- The next section of the file will contain the index (The index is 1962 bytes long. It will start at byte 8
        and end at byte 1699
        4- The final section of the file will contain the location records (the actual data). It starts at byte 1700

        NOTE: RandomAccessFile doesn't have readObject or writeObject, so data has to be stored as discrete data values
        (string, char, int and so on) */

        try (RandomAccessFile rndFile = new RandomAccessFile("locations_rand.dat", "rwd")){
            rndFile.writeInt(locations.size()); // Writing part 1

            /* Each entry in the index is a group of 3 integers (or 12 bytes):
            *   1- Unique ID of the location
            *   2- Index of the start of the location record
            *   3- Length of that record */
            int indexSize = locations.size() * 3 * Integer.BYTES;

            // Calculating where the records start (or index ends), this is the part 1, part 2 and the index size
            int locationStart = (int) (indexSize + rndFile.getFilePointer() + Integer.BYTES);
            rndFile.writeInt(locationStart); // Writing part 2

            // Storing the current file pointer so we can jump back to it when we start to write the index
            long indexStart = rndFile.getFilePointer();

            // Storing the records start point in a new variable and moving the file pointer to it to write the records
            int startPointer = locationStart;
            rndFile.seek(startPointer);

            for (Location location : locations.values()) {
                rndFile.writeInt(location.getLocationID());
                rndFile.writeUTF(location.getDescription());
                StringBuilder builder = new StringBuilder();
                for (String direction : location.getExits().keySet()) {
                    if (!direction.equalsIgnoreCase("Q")) {
                        builder.append(direction);
                        builder.append(",");
                        builder.append(location.getExits().get(direction));
                        builder.append(",");
                        /* Exits are written in a structure like "direction, locationId, direction, locationId,"
                        For instance: N,1,U,2,W,4, */
                    }
                }
                // Writing exits as a String with values separated by commas
                rndFile.writeUTF(builder.toString());

                // Booting the index in memory so it can be written in the file after writing the records
                IndexRecord record = new IndexRecord(startPointer, (int) (rndFile.getFilePointer() - startPointer));
                index.put(location.getLocationID(), record);

                //Update start pointer for the next location
                startPointer = (int) rndFile.getFilePointer();
            }

            /* Once records are written, index (stored while records were being written in the previous for loop)
            is written. Each index entry is composed of 3 integers as described at the beginning of the try block */

            rndFile.seek(indexStart); // Update file pointer to the beginning of the index

            for (Integer locationId : index.keySet()) {
                rndFile.writeInt(locationId);
                rndFile.writeInt(index.get(locationId).getStarByte());
                rndFile.writeInt(index.get(locationId).getLenght());
            }
        }
    }

    // Static block is executed BEFORE the main method of the class
    static {
        // Reading each entry of the index and storing it in memory for use in runtime
        try {
            ra = new RandomAccessFile("locations_rand.dat", "rwd");
            int numLocations = ra.readInt(); // Reading part 1
            long locationStartPoint = ra.readInt(); // Reading part 2

            while (ra.getFilePointer() < locationStartPoint) {
                int locationId = ra.readInt();
                int locationStart = ra.readInt();
                int locationLength = ra.readInt();

                IndexRecord record = new IndexRecord(locationStart, locationLength);
                index.put(locationId, record);
            }
        } catch(IOException e) {
            System.out.println("IOException in static initializer: " + e.getMessage());
        }
    }

    // Locations are no longer in memory, they will be accessed from the file when required using the following method
    public Location getLocation(int locationId) throws IOException {
        //Selecting the index entry for the desired location. Index is loaded into memory from the static initializer
        IndexRecord record = index.get(locationId);

        ra.seek(record.getStarByte()); //Placing file pointer to the record of the desired location

        int id = ra.readInt();
        //ReadUTF knows the string length because writeUTF writes the length of the String and then the string itself
        String description = ra.readUTF();
        String exits = ra.readUTF();
        String[] exitPart = (exits).split(","); // Splitting the exits String into an array

        /* Creating the location with the data taken from the file.
        Exits is sent as null to the constructor, so they can be recovered from the String array of exits */
        Location location = new Location(locationId, description, null);

        // Parsing exits and building the exits map for the location object
        if(locationId != 0) {
            for(int i=0; i<exitPart.length; i++) {
                System.out.println("exitPart = " + exitPart[i]);
                System.out.println("exitPart[+1] = " + exitPart[i + 1]);
                String direction = exitPart[i];
                int destination = Integer.parseInt(exitPart[++i]);
                location.addExit(direction, destination);
            }
        }
        return location;
    }

    @Override
    public int size() {
        return locations.size();
    }

    @Override
    public boolean isEmpty() {
        return locations.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return locations.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return locations.containsValue(value);
    }

    @Override
    public Location get(Object key) {
        return locations.get(key);
    }

    @Override
    public Location put(Integer key, Location value) {
        return locations.put(key, value);
    }

    @Override
    public Location remove(Object key) {
        return locations.remove(key);
    }

    @Override
    public void putAll(Map<? extends Integer, ? extends Location> m) {

    }

    @Override
    public void clear() {
        locations.clear();

    }

    @Override
    public Set<Integer> keySet() {
        return locations.keySet();
    }

    @Override
    public Collection<Location> values() {
        return locations.values();
    }

    @Override
    public Set<Entry<Integer, Location>> entrySet() {
        return locations.entrySet();
    }

    public void close() throws IOException {
        ra.close();
    }
}
