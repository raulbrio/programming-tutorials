package com.timbuchalka;

public class IndexRecord {
    private int starByte;
    private int lenght;

    public IndexRecord(int starByte, int lenght) {
        this.starByte = starByte;
        this.lenght = lenght;
    }

    public int getStarByte() {
        return starByte;
    }

    public void setStarByte(int starByte) {
        this.starByte = starByte;
    }

    public int getLenght() {
        return lenght;
    }

    public void setLenght(int lenght) {
        this.lenght = lenght;
    }
}
