package com.raul;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class Main {
    /* ************ READ AND WRITE BINARY FILES WITH JAVA.NIO CHANNEL AND BUFFER ************** */

    public static void main(String[] args) {
        try(FileOutputStream binFile = new FileOutputStream("data.dat")) {
            FileChannel binChannel = binFile.getChannel(); // Create channel from file

            /* A buffer is essentially a block of memory into which you can write data, which you can then later
            read again. This memory block is wrapped in a NIO Buffer object, which provides a set of methods that
            makes it easier to work with the memory block.

            Buffer can be declared and sized directly like:
                ByteBuffer buffer = ByteBuffer.allocate(100)

            Or can be declared but sized from an array. In this case "wrap" is used instead of "allocate". This implies
            changes made in the array will be reflected in the buffer and vice versa

            Example: Create a CharBuffer with a position of 12, a limit of 54, and a capacity of myArray.length i.e. 100:
                char [] myArray = new char [100];
                CharBuffer charbuffer = CharBuffer.wrap (myArray , 12, 42);*/

            byte[] outputBytes = "Hello World!".getBytes();
            ByteBuffer buffer = ByteBuffer.wrap(outputBytes);

            int numBytes = binChannel.write(buffer); // Write in channel from buffer (reading buffer)
            System.out.println("numBytes written was: " + numBytes);

            ByteBuffer intBuffer = ByteBuffer.allocate(Integer.BYTES); // New byte buffer sized directly with "allocate"
            intBuffer.putInt(245); // Writing data into the buffer
            intBuffer.flip(); // Changing buffer from write mode to read mode. This resets the buffer pointer to the beginning
            numBytes = binChannel.write(intBuffer); // Write in channel from buffer (reading buffer)
            System.out.println("numBytes written was: " + numBytes);

            intBuffer.flip(); //Change buffer to write mode
            intBuffer.putInt(-98765); // Write in buffer
            intBuffer.flip(); //Change buffer to read mode
            numBytes = binChannel.write(intBuffer); // Read from buffer and write it into the channel
            System.out.println("numBytes written was: " + numBytes);

            /////////////////////////////////////////////////////////////////////////////
            /////////////////////////////////////////////////////////////////////////////

            // Now the file can be read with java.NIO

            RandomAccessFile ra = new RandomAccessFile("data.dat", "rwd");
            // Let's write in the buffer (that is now in read mode) without flipping it
            outputBytes[0] = 'k';
            outputBytes[1] = 'i';
            FileChannel channel = ra.getChannel();
            numBytes = channel.read(buffer);
            System.out.println("outputBytes (flip method ommited) = " + new String(outputBytes));

            // Buffer is corrupted and doesn't meet the content of the file. Let's flip it and try again
            buffer.flip();
            numBytes = channel.read(buffer);
            System.out.println("outputBytes (flip method used) = " + new String(outputBytes));

            /* Another way to get the data from the buffer, considered good practice: using buffer.array method, which
            returns the byte array, backing the buffer */
            if (buffer.hasArray()) {
                System.out.println("Byte buffer = " + new String(buffer.array()));
            }

            /////////////////////////////////////////////////////////////////////

            // RELATIVE READ
            // Relative read, which makes necessary to use flip() method:
            intBuffer.flip();
            numBytes = channel.read(intBuffer);
            intBuffer.flip();
            System.out.println("relative read " + intBuffer.getInt());
            intBuffer.flip();
            numBytes = channel.read(intBuffer);
            intBuffer.flip();
            System.out.println("relative read " + intBuffer.getInt());

            channel.position(12);
            //ABSOLUTE READ
            /* Makes unnecessary the flip() method to read the buffer when it's on write mode.
            This is because after the method call the buffer position remain whenever it was before the call */
            intBuffer.flip();
            numBytes = channel.read(intBuffer);
            System.out.println("absolute read " + intBuffer.getInt(0));
            intBuffer.flip();
            numBytes = channel.read(intBuffer);
            System.out.println("absolute read " + intBuffer.getInt(0));

            // Finally, it's good practice to close channel, file stream and any other opened access to file
            channel.close();
            ra.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
