package com.raul;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class CopyFile {
    public static void main(String[] args) {

        try {
            RandomAccessFile ra = new RandomAccessFile("data.dat", "rwd");
            FileChannel channel = ra.getChannel();

            // Actual copy
            RandomAccessFile copyFile = new RandomAccessFile("datacopy.dat","rw");
            FileChannel copyChannel = copyFile.getChannel();
            channel.position(0);
            long numTransferred = copyChannel.transferFrom(channel, 0, channel.size());
            System.out.println("NumTransferred = " + numTransferred);

            channel.close();
            copyChannel.close();
            ra.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
