package com.timbuchalka;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) {

        // Different ways of using paths

        // Getting separator ("\" in Windows, "/" in unix-like systems), as Java code should run on any system regardless
        // of the system where it's developed. There's a method to gather it both in java.io and java.nio
        String separator = File.separator;
        System.out.println(separator);
        separator = FileSystems.getDefault().getSeparator();
        System.out.println(separator);

        // Using paths to access files
        Path path = FileSystems.getDefault().getPath("WorkingDirectoryFile.txt");
        printFile(path);
        Path filePath = Paths.get(".", "files", "SubdirectoryFile.txt");
        printFile(filePath);
        // filePath = Paths.get("C:\\Users\\raul.delbrio\\Documents\\Repositories", "\\java-training", "OutThere.txt");
        filePath = Paths.get("C:" + separator + "Users" + separator + "raul.delbrio" + separator + "Documents" +
                separator + "Repositories", "" + separator + "java-training", "OutThere.txt");
        printFile(filePath);

        // Relative paths, converting relative to absolute and normalizing paths
        filePath = Paths.get(".");
        System.out.println(filePath.toAbsolutePath());
        Path path2 = FileSystems.getDefault().getPath(".", "files", "..", "files", "SubdirectoryFile.txt");
        System.out.println(path2.normalize().toAbsolutePath());
        printFile(path2.normalize());

        // Checking path exists
        Path path3 = FileSystems.getDefault().getPath("thisfiledoesntexist.txt");
        System.out.println("Path2 exists = " + Files.exists(path2));
        System.out.println("Path3 exists = " + Files.exists(path3));

        // Temporary files
        try {
            // First parameter is a prefix for the file naming. Second parameter is the name suffix or extension
            Path tempFile = Files.createTempFile("myapp", ".appext");
            System.out.println("Temporary file path: " + tempFile.toAbsolutePath());
        } catch (IOException e) {
            e.getStackTrace();
        }

        // File Stores
        Iterable<FileStore> stores = FileSystems.getDefault().getFileStores();
        for (FileStore store : stores) {
            System.out.println("Volume name:" + store);
            System.out.println("File store: " + store.name());
        }

        // Copying files with paths
        try {
            Path sourceFile = FileSystems.getDefault().getPath("files", "file1.txt");
            Path copyFile = FileSystems.getDefault().getPath("files", "file2.txt");
            if (!Files.exists(copyFile)) {
                Files.copy(sourceFile, copyFile);
            }
            // If the file being copied already exists, copy will trigger an error. It can be avoided with "replace existing"
            Files.copy(sourceFile, copyFile, StandardCopyOption.REPLACE_EXISTING);
            System.out.println(copyFile + " copied");

            // Moving files
            Path originFile = FileSystems.getDefault().getPath("files", "subdirectoryFile.txt");
            Path destinationFile = FileSystems.getDefault().getPath(".", "movedFile.txt");
            Files.move(originFile, destinationFile);
            System.out.println(destinationFile + " moved");

            // Copying back the file to make program re-run possible
            Files.copy(destinationFile, originFile, StandardCopyOption.REPLACE_EXISTING);

            // Creating files and directories
            Path dirToCreate = FileSystems.getDefault().getPath("dirToCreate");
            Files.createDirectory(dirToCreate);
            System.out.println(dirToCreate + " directory created");
            Path fileToCreate = FileSystems.getDefault().getPath("dirToCreate", "fileToCreate.txt");
            Files.createFile(fileToCreate);
            System.out.println( fileToCreate + " file created");

            // Getting attributes from a file

            // Attributes can be retrieved one by one
            System.out.println("Size: " + Files.size(originFile));
            System.out.println("Last modified: " + Files.getLastModifiedTime(originFile));

            // Or they can be gathered all together and accessed discretely
            BasicFileAttributes attrs = Files.readAttributes(originFile, BasicFileAttributes.class);
            System.out.println("\nSize:" + attrs.size());
            System.out.println("Last modified:" + attrs.lastModifiedTime());
            System.out.println("Created:" + attrs.creationTime());
            System.out.println("Is directory:" + attrs.isDirectory());
            System.out.println("Is regular file:" + attrs.isRegularFile());

            // Deleting files
            System.out.println("Deleting copies... ");
            TimeUnit.SECONDS.sleep(10);
            Files.delete(destinationFile);
            Files.delete(copyFile);
            Files.delete(fileToCreate);
            Files.delete(dirToCreate);

        } catch (IOException e) {
            e.getStackTrace();
        } catch (InterruptedException ie) {
            ie.getStackTrace();
        }
    }

    private static void printFile(Path path) {
        try (BufferedReader fileReader = Files.newBufferedReader(path)) {
            String line;
            while ((line = fileReader.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}
