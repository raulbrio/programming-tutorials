Qty. per Part
2 2N3053 NPN Transistor 							-> OK
1 470 Ohm Resistor (470E-MRS25-1%) 					-> OK
1 2N2905A PNP Transistor 							-> OK
1 1.5 kOhm Resistor (1K5-MRS25-1%)					-> OK
2 1N4148 Diode 										-> OK
1 5.6 kOhm Resistor (5k6-MRS25-1%)					-> OK
2 3.3 Ohm Resistor (3E3-MRS25-1%) 					-> OK
1 47uF/10V Elec. Cap (47uF-10V-EC)					-> OK
1 22 Ohm Resistor (22E-MRS25-1%) 					-> OK
1 1000uF/50V Elec. Cap (1000uF-50V-EC)				-> OK
5 SOLDEREYE 1MM (for Input, Speaker and 9V supply)