#include <atmel_start.h>
#include <avr/io.h>
#include <avr/delay.h>

#define delay(a) (5000 + (a * 1000))

int main(void)
{
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();
	
	// declare and initialize the scroll delay_count
	unsigned long delay_count = 10000;
	
	// declare a variable for the speed increase
	unsigned long increase = 0;
	
	// declare a variable for the polarity
	unsigned char polarity = 0;
	
	// initialize port pins
	DDRA = 0xFF; // port A as output
	PINB = 0x00; // port B as input
	
	// example of port C with low nibble as output and higher nibble as input: DDRC = 0b00001111;

	/* Replace with your application code */
	
	while (1) {
		
		// read the buttons
		increase = PINB;
		
		// set the polarity
		if (increase > 127)
		{
			increase -= 127;
			polarity = 1;
		}
		else
		{
			polarity = 0;
		}
		
		//set the delay count
		delay_count = delay(increase);
		
		// scroll effect
		for (int i = 1; i <= 128; i = i*2)
		{
			if (polarity)
			{
				PORTA = ~i;
			}
			else
			{
				PORTA = i;
			}
			_delay_loop_2(delay_count);
		}
		for (int i = 128; i > 1; i -= i/2)
		{
			if (polarity)
			{
				PORTA = ~i;
			}
			else
			{
				PORTA = i;
			}
			_delay_loop_2(delay_count);
		}
	}
	return 1;
}