#include <atmel_start.h>
#include <avr/signal.h>
#include <avr/io.h>
#include <util/delay.h>
#include <inttypes.h>

// Defining mask for pins in port B so button 0 can be use to trigger interrupts
#define PINB_MASK ((1<<PINB0));

void initializer()
{	
	// Set port A as output for LEDs, port B as input for buttons
	DDRA = 0xFF;
	DDRB |= 0x00;
	
	// Enable interrupts on Port B
	PCICR |= (1<<PCIE0);
	
	// Define which pins in Port B will be used to trigger interrupts
	PCMSK0 |= (1<<PCINT0);
	
	// Finally Set Enable Interrupt (sei()) is called to switch all interrupts on. To disable interrupts, "cli()" is used
	sei();
}

// Signal code provided to be called when the interrupt occurs
ISR(PCINT0_vect)
{
	pin_change_interrupt();
}

void pin_change_interrupt(void)
{
	PORTA = ~0xFF;
	delay_func();
	PORTA = ~0x00;
}

int main(void)
{
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();
	
	// Initializer for interrupt yadda yadda
	initializer();
	
	int output_byte = 1;
	bool going_up = true, extra_pause = false;
	
	/* Replace with your application code */
	while (1) {
		if (going_up)
		{
			output_byte *= 2;
		}
		else
		{
			output_byte /= 2;
		}
		
		PORTA = ~output_byte;
		
		delay_func();
		
		switch (output_byte)
		{
			case 1:
			going_up = true;
			break;
			case 128:
			going_up = false;
			break;
			default:
			break;
		}
	}
	
}

void delay_func(){
	for (int j = 0; j < 10; j ++)
	{
		_delay_loop_2(30000);
	}
}
