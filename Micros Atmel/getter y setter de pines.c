int set_PORT_bit(volatile uint8_t *port, int position, int value)
{
	// Sets or clears the bit in position 'position' 
	// for the port 'port'
	// either high or low (1 or 0) to match 'value'.
	// Leaves all other bits unchanged.
	if (value == 0)
	{
		*port &= ~(1 << position);
	}
	else
	{
		*port |= (1 << position);
	}

	return 1;
}

int get_PORT_bit(volatile uint8_t *port, int position)
{
	if ((*port & (1 << position)) == 0) //if pin check return zero, pin is not set
	{	
		return FALSE;
	}
	else //pin check returned other value than zero; pin is set
	{	
		return TRUE;
	}
}