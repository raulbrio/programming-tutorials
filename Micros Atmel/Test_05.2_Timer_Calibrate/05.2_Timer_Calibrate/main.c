#include <atmel_start.h>

int main(void)
{
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();
	
	// initialize port pins
	DDRA = 0xFF; // port A as output
	
	use_synchronizer(); //call timer function
	
	/* Replace with your application code */
	while (1)
	{
		PORTA = ~0xFF; // All lights on to show it reached main loop
	}
}

void use_synchronizer()
{
	// If this function works correctly, OSCCAL_calibration() should complete and light 0 should start blinking
	OSCCAL_calibration();
	
	uint16_t delay_lenght = 30000;
	bool on_or_off = true;
	
	
	/* Replace with your application code */
	while (1)
	{
		for (int i = 0; i < 10; i ++)
		{
			_delay_loop_2(delay_lenght);
		}
		
		if (on_or_off)
		{
			PORTA = ~0x00;
			on_or_off = false;
		}
		else
		{
			PORTA = ~0x01;
			on_or_off = true;
		}
	}
}