#include <atmel_start.h>
#include "avr\iom2560.h"

int main(void)
{
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();
	
	/* Set PORTA to output mode */
	DDRA = 0xFF;
	
	/* Set Fast PWM mode and CLK/256 prescaler
	
	We want to set bits 6, 3 and 2 (01001100 = 0x4C) without affecting the other bits (Using bitwise OR)
	
	TCC0RA |= 0x4C;
	
	So the bitwise OR is:
	
	TCC0RA = xxxxxxxx = don't now nor need to
	0x4C   = 01001100
	-----------------
	OR     = x1xx11xx = bits 6, 3 and 2 are set; the rest aren't modified
	
	So 
		TCC0RA |= 0x4C;
	is the same as 
		TCCR0A |= (1<<WGM01) | (1<<WGM00) | (4<<CS00);
		
	but it's easier to read what is being set (Waveform Generation bits to true and clock prescaler to 4)
	
	*/
	
	TCCR0A |= (1<<WGM01) | (1<<WGM00) | (4<<CS00);

	/* Replace with your application code */
	while (1) {
		
		/*Check Waveform Generator
		
		WGM01	WGM00	Mode
		-----------------------
		0		0		Normal
		0		1		PWM, phase correct
		1		0		CTC
		1		1		Fast PWM
		
		*/
		
		if (!(TCCR0A & WGM01) && !(TCCR0A & WGM00))
		{
			// normal mode
			PORTA = ~0x01;
		}
		else if (!(TCCR0A & WGM01) && (TCCR0A & WGM00))
		{
			// PWM, phase correct mode
			PORTA = ~0x04;
		}
		else if ((TCCR0A & WGM01) && !(TCCR0A & WGM00))
		{
			// CTC mode
			PORTA = ~0x08;
		}
		else if ((TCCR0A & WGM01) && (TCCR0A & WGM00))
		{
			// Fast PWM mode
			PORTA = ~0x10;
		}
	}
}
