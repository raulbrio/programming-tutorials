/*
 * useful_func.h
 *
 * Created: 28/12/2020 13:55:51
 *  Author: raul.delbrio
 */ 
#include <util/delay_basic.h>


#ifndef USEFUL_FUNC_H_
#define USEFUL_FUNC_H_
	void OSCCAL_calibration(void);
	void USARTinit();
	void use_synchronizer();
	void timer0_init();
	void timer1_init();
	void timer2_init();
#endif /* USEFUL_FUNC_H_ */

void OSCCAL_calibration(void){
	unsigned char calibrate = false;
	int temp;
	unsigned char tempL;
	
	/*						**** Registers related to counters ****
	TIMSK = interrupts register. Bits 0 and 1 are related to timer 0
	TIFR = interrupts flags register. Bits 0 and 1 are alloted to timer 0, simillarly to TIMSK. For instance:
	- The 0th bit (TOV0 bit) is set (one) whenever TIMER0 overflows. 
	- This bit is reset (zero) whenever the Interrupt Service Routine (ISR) is executed. 
	- If there is no ISR to execute, we can clear it manually by writing one to it.
	*/
	
	CLKPR = (1<<CLKPCE);					// set Clock Prescaler Change Enable
	CLKPR = (1<<CLKPS1) | (1<<CLKPS0);
	TIMSK2 = 0;								// disable OCIE2A and TOIE2
	//TODO: Adapt code for 11.0592MHz crystal
	ASSR = (1<<AS2);						// select asynchronous operaton of timer2 (32768kHz)
	OCR2A = 200;							// set timer2 compare value
	TIMSK0 = 0;								// delete any interrupt sources
	TCCR1B = (1<<CS10);						// start timer1 with no prescaling
	TCCR2A = (1<<CS20);						// start timer2 with no prescaling
	
	while((ASSR & 0x01) | (ASSR & 0x04)); // wait for TCN2UB and TCR2UB to clear
	
	// wait for external crystal to stabilize
	for(int i = 0; i < 10; i++)
	{
		_delay_loop_2(1000);
	}
	
	while(!calibrate)
	{
		cli();			// disable global interrupt
		TIFR1 = 0xFF;	// delete TIFR1 flags
		TIFR2 = 0xFF;	// delete TIFR2 flags
		TCNT1H = 0;		// clear timer1 counter
		TCNT1L = 0;
		TCNT2 = 0;		// clear timer2 counter
		
		while ( ! (TIFR2 && (1<<OCF2A)) );	// wait for timer2 to reach the count (compareflag)
		
		TCCR1B = 0;	// stop timer1
		sei();	// enable global interrupt
		
		// if timer1 overflows, set the temp to 0xFFFF
		if ( (TIFR1 && (1<<TOV1)) )
		{
			temp = 0xFFFF;
		}
		else
		{
			// read out the timer1 counter value
			tempL = TCNT1L;
			temp = TCNT1H;
			temp = (temp << 8);
			temp += tempL;
		}
		
		if (temp > 6250)
		{
			OSCCAL--; // RC oscillator runs too fast, decrease OSCCAL
		}
		else if (temp < 6120)
		{
			OSCCAL++; // RC oscillator runs too slow, increase OSCCAL
		}
		else
		{
			calibrate = true; // start timer1
		}
		TCCR1B = (1<<CS10);
	}
}

void USARTinit(){
	CLKPR = (1<<CLKPCE);
	CLKPR = (1<<CLKPS1);
	UBRR0H = 0;
	UBRR0L = 12;
	UCSR0A = (1<<U2X0);
	UCSR0B = (1<<RXEN0)|(1<<TXEN0)|(0<<RXCIE0)|(0<<UDRIE0);
	UCSR0C = (0<<UMSEL00)|(0<<UPM00)|(0<<USBS0)|(3<<UCSZ00)|(0<<UCPOL0);
}