#include <atmel_start.h>
#include <useful_func.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>
#include <inttypes.h>

int main(void)
{
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();
	
	// initialize port pins
	DDRA = 0xFF; // port A as output
	PINB = 0x00; // port B as input
	
	char increase = 0;
	
	/* Replace with your application code */
	while (1)
	{
		// read the buttons
		increase = ~PINB;
		
		if (increase > 0)
		{
			switch(increase)
			{
				case 1: // button 0
				PORTA = ~0x00;
				use_synchronizer();
				break;
				
				case 2: // button 1
				PORTA = ~0x00;
				use_timer0();
				break;
				
				case 4: // button 2
				PORTA = ~0x00;
				use_timer1();
				break;
				
				case 8: // button 3
				PORTA = ~0x00;
				use_timer2();
				break;
				
				default:
				break;
			}
		}
		else
		{
			PORTA = ~0xFF; // All lights on until input is chosen
		}
	}
}
