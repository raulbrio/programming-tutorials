/*
 * CFile1.c
 *
 * Created: 30/12/2020 11:26:18
 *  Author: raul.delbrio
 */ 
#include <atmel_start.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>
#include <inttypes.h>


// global variable to count the number of overflows
volatile uint8_t tot_overflow;

void use_timer0(void)
{
	/* Exercise: Given a XTAL of 11.0592MHz, flash LED0 every 50ms. Let's do the math:
	
	11.0592mhz = 11059.2KHz = 11059200hz
	50ms = 0,05s = 20 ciclos/seg (1 ciclo/s == 1000ms)

	Divisor sin prescaler:
	11059200 / 20 = 552960

	Con un timer de 8 bits se puede contar hasta 256. Con uno de 16 bits, hasta 65535. 
	Para hacerlo con timer0 (8 bits) sin hacer overflow hasta el infinito, hay que usar un prescaler. 
	Por ejemplo, con prescaler 1024:

	11059200 / 1024 = 10800

	Con lo que el reloj quedaria en 10,8KHz
	Con ese reloj, obtener un ciclo de 50ms ser�a:
	10800 / 20 = 540

	O lo que es lo mismo, 

	a) dejar que el timer haga overflow 2 veces (256*2 = 512) y 
	b) contar 28 m�s (540-512 = 12)
	
	*/
	
	// initialize timer
	timer0_init();
	
	// loop forever
	while(1)
	{
		// check if no. of overflows = 2
		if (tot_overflow >= 2)
		{
			// check if the timer count reaches 28
			if (TCNT0 >= 28)
			{
				PORTA ^= (1 << 0);    // toggles the led
				TCNT0 = 0;            // reset counter
				tot_overflow = 0;     // reset overflow counter
			}
		}
	}
}

// initialize timer, interrupt and variable
void timer0_init()
{
	// C1:: timer 0 mode 2 - CTC (for using with interruptions)
	// Comment this to use normal (overflow) mode
	TCCR0A = (1<<WGM01);
	
	// set up timer with prescaler = 1024
	TCCR0B |= (1<<CS02 | 1<<CS00);
	
	// initialize counter
	TCNT0 = 0;
	
	// enable overflow interrupt
	TIMSK0 |= (1<<TOIE0);
	
	// enable global interrupts
	sei();
	
	// initialize overflow counter variable
	tot_overflow = 0;
}

// TIMER0 overflow interrupt service routine
// called whenever TCNT0 overflows
ISR(TIMER0_OVF_vect)
{
	// keep a track of number of overflows
	tot_overflow++;
}