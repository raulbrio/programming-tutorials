/*
 * CFile4.c
 *
 * Created: 30/12/2020 11:27:03
 *  Author: raul.delbrio
 */ 
#include <atmel_start.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>
#include <inttypes.h>

void use_synchronizer()
{
	// If this function works correctly, OSCCAL_calibration() should complete and light 0 should start blinking
	OSCCAL_calibration();
	
	uint16_t delay_lenght = 30000;
	bool on_or_off = true;
	
	
	/* Replace with your application code */
	while (1)
	{
		for (int i = 0; i < 10; i ++)
		{
			_delay_loop_2(delay_lenght);
		}
		
		if (on_or_off)
		{
			PORTA = ~0x00;
			on_or_off = false;
		}
		else
		{
			PORTA = ~0x01;
			on_or_off = true;
		}
	}
}