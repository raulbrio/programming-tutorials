#include <atmel_start.h>
#include <avr/io.h>
#include <avr/delay.h>

int main(void)
{
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();
	DDRA = 0xFF;
	int wait_delay = 30000, output_byte = 1;
	bool going_up = true, extra_pause = false;

	/* Replace with your application code */
	while (1) {
		
		if (going_up) 
		{
			output_byte *= 2;
		}
		else 
		{
			output_byte /= 2;
		}
		
		PORTA = ~output_byte;
		
		for (int j = 0; j < 10; j ++) 
		{
			_delay_loop_2(wait_delay);
		}
		
		switch (output_byte) 
		{
			case 1:
				going_up = true;
				extra_pause = true;
				break;	
			case 2:
			case 64:
				wait_delay = 20000;
				break;
			case 4:
			case 32:
				wait_delay = 15000;
				break;
			case 8:
			case 16:
				wait_delay = 3000;
				break;
			case 128:
				going_up = false;
				extra_pause = true;
				break;
			default:
				break;
		}
		
		if (extra_pause) 
		{
			for (int j = 0; j < 10; j ++) 
			{
			_delay_loop_2(wait_delay);
			}
			extra_pause = false;
		}
	}
	return 1;
}
